from django.contrib.admin import AdminSite
from django.contrib.auth.models import *
from django.contrib.auth.admin import GroupAdmin, UserAdmin
from data.filters import *
from data.forms import RequiredInlineFormset
from adminfilters.multiselect import UnionFieldListFilter
from ext_lib.adminactions import merge
from utils.excel import ExcelCurryCook
from rainbow_remarks.models import *
from .models import *
import autocomplete_light

APP_NAME = 'data'


class HiderAdmin(admin.ModelAdmin):
    def get_model_perms(self, request):
        return {}


class ReferralInline(admin.TabularInline):
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == "referred_by":
            kwargs["queryset"] = Client.objects.filter().order_by('first_name')
        return super(ReferralInline, self).formfield_for_foreignkey(db_field, request, **kwargs)

    name = "referral"
    form = autocomplete_light.modelform_factory(Referral,
                                                fields=["date_created", "client", "referred_by", "relationship"])
    model = Referral
    extra = 0
    fk_name = 'client'
    position = "top"


class PhoneNumberInline(admin.TabularInline):
    name = "phone_number"
    model = PhoneNumber
    extra = 0
    fk_name = 'client'
    position = "top"


class DealerInline(admin.TabularInline):
    name = "dealer"
    form = autocomplete_light.modelform_factory(DealerClientPair, fields=["date_assigned", "client", "dealer"])
    # def formfield_for_foreignkey(self, db_field, request, **kwargs):
    #     if db_field.name == "referred_by":
    #         kwargs["queryset"] = Associate.objects.filter(is_active=True).order_by('last_name')
    #     return super(ReferralInline, self).formfield_for_foreignkey(db_field, request, **kwargs)

    model = DealerClientPair
    extra = 0
    formset = RequiredInlineFormset


class AssociateAdmin(admin.ModelAdmin):
    fields = Associate.field_order
    form = autocomplete_light.modelform_factory(Associate, fields=fields)
    list_display = ["get_name"]
    search_fields = ['first_name', 'last_name']
    list_filter = ("is_active", )

    def get_name(self, obj):
        return str(obj)
    get_name.short_description = "Name"
    get_name.admin_order_field = "-first_name"

    def activate(self, request, queryset):
        for associate in queryset:
            associate.is_active = True
            associate.save()

    def deactivate(self, request, queryset):
        for associate in queryset:
            associate.is_active = False
            associate.save()

    actions = [merge.merge, activate, deactivate]

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == "city":
            kwargs["queryset"] = City.objects.order_by('name')
        return super(AssociateAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    readonly_fields = ('preview', )


class ServiceAppointmentAdmin(admin.ModelAdmin):
    list_per_page = 50

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == "client":
            kwargs["queryset"] = Client.objects.order_by('first_name')
        elif db_field.name in ["created_by", "demo_done_by", "assistant"]:
            kwargs["queryset"] = Associate.objects.filter(is_active=True).order_by('first_name')
        return super(ServiceAppointmentAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    fields = ServiceAppointment.field_order
    form = autocomplete_light.modelform_factory(ServiceAppointment, fields=fields)
    default_list_display = ['date_created', 'demo_date', 'created_by', 'client', 'get_referrals', 'get_client_dealers',
                            'demo_done_by', 'get_type', 'get_status', 'get_city']
    list_display = default_list_display
    search_fields = ('client__first_name', 'client__last_name', 'demo_done_by__last_name', 'demo_done_by__first_name')

    excel_cook = ExcelCurryCook(filename='service-appointment.xls')
    export_excel = excel_cook.curry_export_action()
    actions = [export_excel]

    list_filter = (
        ('demo_date', DateRangeFilter),
        ('service_status', UnionFieldListFilter),
        ('program_joined',  UnionFieldListFilter),
        ('service_type', UnionFieldListFilter),
        'will_follow_up',
        ('created_by', DealerFilter),
        ('demo_done_by', DealerFilter),
    )

    def get_referrals(self, obj):
        referral_data = Referral.objects.filter(client=obj.client_id)
        return ','.join([str(referral.referred_by) for referral in referral_data])
    get_referrals.short_description = 'Referred By'

    def get_type(self, obj):
        return obj.service_type
    get_type.short_description = 'Type'
    get_type.admin_order_field = 'service_type'

    def get_status(self, obj):
        return obj.service_status
    get_status.short_description = 'Status'
    get_status.admin_order_field = 'service_status'

    def show_details_link(self, obj):
        return '<a href="/asview/serviceappointment/{}">Details</a>'.format(obj.id)
    show_details_link.allow_tags = True
    show_details_link.short_description = ''

    def show_edit_link(self, obj):
        return '<a href="{}">Edit</a>'.format(obj.id)
    show_edit_link.allow_tags = True
    show_edit_link.short_description = ''

    def get_list_display(self, request):
        self.list_display = ['show_details_link', 'show_edit_link'] + self.default_list_display

        if not request.user.has_perm(APP_NAME + "." + ADMIN_BASE_EDIT):
            self.list_display.remove('show_edit_link')
            self.readonly_fields = self.fields

        if not request.user.has_perm(APP_NAME + "." + VIEW_DETAILS):
            self.list_display.remove('show_details_link')

        return super(ServiceAppointmentAdmin, self).get_list_display(request)


class RemarkAdmin(admin.ModelAdmin):
    list_filter = ["is_removed", 'user', 'data_origin']
    search_fields = ['object_pk']


class ClientAdmin(admin.ModelAdmin):
    list_per_page = 50

    def get_readonly_fields(self, request, obj=None):
        self.inlines = [PhoneNumberInline]
        if request.user.has_perm(APP_NAME + "." + ADMIN_INLINE_EDIT):
            self.inlines.insert(0, ReferralInline)
            self.inlines.append(DealerInline)
        return super(ClientAdmin, self).get_readonly_fields(request, obj)

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == "city":
            kwargs["queryset"] = City.objects.order_by('name')
        return super(ClientAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    fields = Client.field_order
    form = autocomplete_light.modelform_factory(Client, fields=fields)
    form.Meta.widgets = {'is_from_leads_data': forms.HiddenInput()}
    default_list_display = ['first_name', 'last_name', 'get_phone_numbers', 'get_dealers', 'get_referrals',
                            'city', 'original_as']
    list_display = default_list_display
    search_fields = ['last_name', 'first_name']
    actions = [merge.merge]
    list_filter = (
                   CityFilter,
                   ClientsDealersFilter,
                   RemarkTypeFilter,
                   OriginalAsFilter,
                   ('file_origin', FileOriginFilter),
                   "is_a_rainbow_owner",
    )

    def get_referrals(self, obj):
        return ','.join([str(referral.referred_by) for referral in Referral.objects.filter(client=obj.id)])
    get_referrals.short_description = 'Referred By'

    # def show_referrals:
    def show_details_link(self, obj):
        return '<a href="/asview/client/{}">Details</a>'.format(obj.id)
    show_details_link.allow_tags = True
    show_details_link.short_description = ''

    def show_edit_link(self, obj):
        return '<a href="{}">Edit</a>'.format(obj.id)
    show_edit_link.allow_tags = True
    show_edit_link.short_description = ''

    def get_list_display(self, request):
        self.list_display = ['show_details_link', 'show_edit_link'] + self.default_list_display

        if not request.user.has_perm(APP_NAME + "." + ADMIN_BASE_EDIT):
            self.list_display.remove('show_edit_link')
            self.readonly_fields = self.fields
        else:
            self.readonly_fields = []

        if not request.user.has_perm(APP_NAME + "." + VIEW_DETAILS):
            self.list_display.remove('show_details_link')

        return super(ClientAdmin, self).get_list_display(request)


class PermissionAdmin(admin.ModelAdmin):
    model = Permission
    fields = ['name']


class RainbowAdminSite(AdminSite):
    site_header = "Rainbow Business Intelligence System"


class PhoneNumberAdmin(admin.ModelAdmin):
    list_display = ['client', 'number']
    search_fields = ['number']

rainbow_admin_site = RainbowAdminSite(name='index')
rainbow_admin_site.register(Client, ClientAdmin)
rainbow_admin_site.register(ServiceAppointment, ServiceAppointmentAdmin)
rainbow_admin_site.register(Associate, AssociateAdmin)
rainbow_admin_site.register(City, HiderAdmin)
rainbow_admin_site.register(AssociatePosition)
rainbow_admin_site.register(ServiceType)
rainbow_admin_site.register(ServiceStatus)
rainbow_admin_site.register(ClientConcern)
rainbow_admin_site.register(Vehicle)
rainbow_admin_site.register(UserLoginHistory)
rainbow_admin_site.register(Remark, RemarkAdmin)
rainbow_admin_site.register(Referral)
rainbow_admin_site.register(Permission)
rainbow_admin_site.register(User, UserAdmin)
rainbow_admin_site.register(Group, GroupAdmin)
rainbow_admin_site.register(Program)
rainbow_admin_site.register(RemarkType)
rainbow_admin_site.register(PhoneNumber, PhoneNumberAdmin)
