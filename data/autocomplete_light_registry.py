import autocomplete_light.shortcuts as al
from .models import Client, Associate, City

__author__ = 'Geryl'

al.register(Client,
            search_fields=['^first_name', '^last_name'],
            split_words=True,
            choices=Client.objects.all(),
            attrs={
                'placeholder': 'Search Client Name Here',
                'data-autocomplete-minimum-characters': 3,
            },
            widget_attrs={
                'data-widget-maximum-values': 3,
                'class': 'modern-style',
            },
            )

al.register(Associate,
            search_fields=['^first_name', '^last_name'],
            split_words=True,
            choices=Associate.objects.all(),
            attrs={
                'placeholder': 'Search Associate Name Here',
                'data-autocomplete-minimum-characters': 1,
            },
            widget_attrs={
                'data-widget-maximum-values': 3,
                'class': 'modern-style',
            },
            )

al.register(City,
            search_fields=['^name'],
            attrs={
                # This will set the input placeholder attribute:
                'placeholder': 'Search City Name Here',
                'data-autocomplete-minimum-characters': 1,
            },
            widget_attrs={
                'data-widget-maximum-values': 3,
                'class': 'modern-style',
            },
            )
