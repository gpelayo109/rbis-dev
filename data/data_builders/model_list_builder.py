import logging
import types
from django.apps import apps
from data.data_builders.qckimprt_builder import GodsownModelBranch, GodsownParentLeaf, get_model_from_db
from exceptions import GodsownException
#TODO: Rename to just model_list.py


class PreviewTableNotAssigned(GodsownException):
    def __init__(self, model):
        self.message = "{} was not assigned a table.".format(model)


class ModelTableData:
    model_type = None
    output_fields = None
    data = None

    @property
    def model_name(self):
        return self.model_type._meta.verbose_name.title


class SearchModelTableData(ModelTableData):
    class ModelTableMessages:
        initial = None
        no_results = None

        def __init__(self):
            self.initial_msg = "Search"
            self.no_results = "Nothing Found"

    query = None
    search_fields = None
    messages = None

    def __init__(self):
        self.messages = self.ModelTableMessages()


class ModelTableDataFactory:
    model_data_tables = None
    raw_data = None
    header_dict = None
    skipped_row_count = None
    __last_row_is_skipped = None

    def __init__(self, data, header_dict):
        self.raw_data = data
        self.model_data_tables = {}
        self.header_dict = header_dict
        self.__last_row_is_skipped = True
        self.skipped_row_count = 0
        for headers in header_dict.keys():
            self.model_data_tables.setdefault(headers, [])

    def create_set(self):
        for row in self.raw_data:
            self._extract_by_hierarchy(row)
            if self.__last_row_is_skipped:
                self.skipped_row_count += 1
            self.__last_row_is_skipped = True
        table_data_list = []
        for full_model_name in self.header_dict.keys():
            fixed_data = set(self.model_data_tables[full_model_name])
            if fixed_data:
                app_name, model_name = full_model_name.split('.')
                new_table_data = ModelTableData()
                new_table_data.data = fixed_data
                new_table_data.model_type = apps.get_model(app_name, model_name)
                fixed_headers = []
                for field_name in self.header_dict[full_model_name]:
                    fixed_headers.append(new_table_data.model_type._meta.get_field(field_name).verbose_name.title())
                new_table_data.output_fields = fixed_headers
                table_data_list.append(new_table_data)
        return table_data_list

    def _extract_by_hierarchy(self, data):
        fk_data = dict()
        if isinstance(data, GodsownModelBranch):
            for foreign_key in data.fk:
                fk_dict = self._extract_by_hierarchy(foreign_key)
                if fk_dict:
                    fk_data.update(fk_dict)

            found_model = get_model_from_db(data)
            if not found_model:
                model_data = self._extract_data(data, fk_data)
            else:
                model_data = {data.linked_field_name: str(data.model)}
            if data.dependencies:
                for dep in data.dependencies:
                    self._extract_by_hierarchy(dep)
            return model_data
        elif isinstance(data, GodsownParentLeaf):
            return {data.linked_field_name: str(data.model)}

    def _extract_data(self, data, fk_data=None):
        app_name = data.model._meta.app_label
        model_name = data.model.__class__.__name__.lower()
        full_app_name = "{}.{}".format(app_name, model_name)
        model_row = []
        try:
            for field_name in self.header_dict[full_app_name]:
                if field_name in fk_data.keys():
                    model_row.append(fk_data[field_name])
                else:
                    try:
                        model_row.append(getattr(data.model, field_name))
                    except AttributeError:
                        logging.error("Can't find {}.{}".format(data.related_name, field_name))
            self.model_data_tables[full_app_name].append(tuple(model_row))
            self.__last_row_is_skipped = False
        except KeyError:
            raise PreviewTableNotAssigned(full_app_name)
        try:
            displayed_field = str(data.model)
        except Exception:
            displayed_field = "NULL MODEL"

        return {data.linked_field_name: displayed_field}


def get_data_args(request, model, search_fields,
                  max_entries=10,
                  title="Model Table",
                  initial_msg="Search for model entry",
                  no_results_msg="Sorry, we can't find your model entry"):
    column_headers = []

    for field in model.displayed_fields:
        datum_point = getattr(model(), field)
        if isinstance(datum_point, types.MethodType):
            column_headers.append(datum_point.short_description)
        else:
            column_headers.append(model._meta.get_field(field).verbose_name.title())

    mdl_dta = SearchModelTableData()
    mdl_dta.model_type = model
    mdl_dta.query = request.POST.get('query', '')
    mdl_dta.search_fields = search_fields
    mdl_dta.output_fields = column_headers
    mdl_dta.max_entries = max_entries
    mdl_dta.messages.initial = initial_msg
    mdl_dta.messages.no_results = no_results_msg

    data_args = {
        "title": title,
        "model_data": mdl_dta,
        "max_entries": max_entries
    }

    return data_args
