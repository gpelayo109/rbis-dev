import copy
import os
import logging
import configparser
from django.apps import apps
from django.db.models import Model
from django.contrib.auth.models import ContentType, User
from django.core.exceptions import ObjectDoesNotExist
from django_comments.models import Site
from rainbow_db import settings
from utils import rainbow_format
from exceptions import GodsownException

COLUMN_DELIMITER = ","
CLASS_FIELD_DELIMITER = "."

EXCEL_TYPE_FOLDER = os.path.join(settings.BASE_DIR, "data", "data_builders", "settings", "excel-types")
HEADER_NAMES_FOLDER = os.path.join(settings.BASE_DIR, "data", "data_builders", "settings", "headers")
logging.basicConfig(level=logging.CRITICAL)


class FieldNotInFKFieldsException(GodsownException):
    def __init__(self, field_name):
        self.message = "Foreign Key Field [{}] not assigned under the FKFields section " \
                       "in the excel type setting.".format(field_name)


class RelatedNameNotDependantException(GodsownException):
    message = None

    def __init__(self, field, model):
        self.message = "'{}' not assigned as dependant for '{}'.".format(field, model)


class RelatedNameHasNoTypeException(GodsownException):
    message = None

    def __init__(self, Model):
        self.message = "No datatype defined for '{}'.".format(Model)


class NoFieldListException(GodsownException):
    message = "The Field List is not populated"


class InputRowSizeMismatchException(GodsownException):
    message = "Input's row size doesn't match the row size of the headers"


class ModelDoesntExistException(GodsownException):
    def __init__(self, msg):
        self.message = msg


class RelModel:
    def show_values(self):
        display_list = []
        for field_str in self.__dict__.keys():
            display_list.append("{}: {}".format(field_str, getattr(self, field_str)))
        return "\n".join(display_list)


class RelFieldTable:
    def show_table(self):
        display_list = []
        for type_str in self.__dict__.keys():
            line = "=={}==\n{}".format(type_str, getattr(self, type_str).show_values())
            display_list.append(line)
        return "\n".join(display_list)


class TemplateList:
    def __str__(self):
        display_list = []
        for type_str in self.__dict__.keys():
            line = "=={}==\n{}".format(type_str, getattr(self, type_str))
            display_list.append(line)
        return "\n".join(display_list)


class GodsownModelNode:
    model = None
    model_type = None
    related_name = None
    linked_model_name = None
    linked_field_name = None
    is_new_entry = True


class GodsownModelBranch(GodsownModelNode):
    fk = None
    dependencies = None
    used_fields = None

    def __init__(self, model, model_type, related_name, fk, dependencies, used_fields, is_old_entry=False):
        self.model = model
        self.model_type = model_type
        self.related_name = related_name
        self.fk = fk
        self.dependencies = dependencies
        self.used_fields = used_fields

    def set_linked_data(self, model, field):
        self.linked_model_name = model
        self.linked_field_name = field

    def __str__(self):
        if self.dependencies:
            dep_str = ", {}".format([str(dep) for dep in self.dependencies])
        else:
            dep_str = ""

        if self.fk:
            fk_str = "->{}".format([str(dep) for dep in self.fk])
        else:
            fk_str = ""

        return "({}{}{})".format(self.model, fk_str, dep_str)


class GodsownParentLeaf(GodsownModelNode):

    def __init__(self, model: Model, related_name, linked_model_name, linked_field_name):
        self.model = model
        self.linked_model_name = linked_model_name
        self.linked_field_name = linked_field_name
        self.related_name = related_name


class RowVariableType:
    FK_VARIABLE = 0


class SettingsLibrary:
    PARENT_KEYWORD = "@parent@"

    model_templates = None

    class FileSettings:
        file_name = None
        year = None
        date_format = None
        date_order = None
        no_year_fields = None

        def __init__(self):
            self.date_format = {}
            self.date_order = {}

    class SystemSettings:
        required_apps = None

        def __init__(self):
            self.required_apps = []

    class MainSettings:
        registered_columns = None
        required_columns = None
        header_to_field_dict = None
        related_name_types = None
        fk_field_table = None
        template_model_values = None
        model = None

        def __init__(self):
            self.registered_columns = []
            self.required_columns = {}
            self.template_model_values = []
            self.header_to_field_dict = {}
            self.related_name_types = {}
            self.fk_field_table = RelFieldTable()

    class DependencySettings:
        defaults = None
        row_unique_values = None
        cell_unique_values = None
        row_variables = None
        links = None
        cu_doci_dict = None
        ru_doci_dict = None
        query_fields = None

        def __init__(self):
            self.defaults = {}
            self.row_unique_values = {}
            self.cell_unique_values = {}
            self.row_variables = {}
            self.links = {}
            self.query_fields = {}

    dependency = None
    main = None
    system = None
    file = None

    def __init__(self, app_names):
        self.dependency = self.DependencySettings()
        self.main = self.MainSettings()
        self.system = self.SystemSettings()
        self.system.required_apps = app_names
        self.file = self.FileSettings()
        self.model_templates = TemplateList()

    def convert_str_to_model(self, related_name):
        is_success = False
        model = None
        if related_name in self.main.related_name_types.keys():
            model_name = self.main.related_name_types[related_name]
        else:
            logging.error("Related name ({}) wasn't assigned in the settings.".format(related_name))
            model_name = related_name
        for app_name in self.system.required_apps:
            try:
                model = apps.get_model(app_name, model_name)
                is_success = True
            except LookupError:
                pass
        #Todo Add exception if model is invalid/not found in
        if not is_success:
            raise ModelDoesntExistException("Can't convert string [{}] to model. It doesn't exist.".format(model_name))
        else:
            return model

    #TODO: Remove Parent Entry (Not Used)
    def create_template_model(self, related_name, current_entry=None, parent_entry=None, fk_field_values: list=None,
                              parent_fk_fields=None):
        #TODO Add parentheses to model instantiation (CUrrently assisgns type, not instance)
        if hasattr(self.model_templates, related_name):
            template_model = getattr(self.model_templates, related_name)
        elif related_name == SettingsLoader.MAIN_RELATED_NAME:
            template_model = self.main.model()
        else:
            data_type = self.main.related_name_types[related_name]
            template_model = self.convert_str_to_model(data_type)()
        #TODO change to be compatible with related names
        template_related_name = template_model.__class__.__name__.lower()
        vars_dict = self.dependency.row_variables.get(template_related_name, {})
        #TODO: Rename var to row_variable
        for var_field in vars_dict:
            var = vars_dict[var_field]
            row_var_type = self.get_row_variable(var, current_entry)

            if row_var_type == RowVariableType.FK_VARIABLE:
                for fk_obj in parent_fk_fields:
                    related_name = var.split(":")[1].strip().lower()
                    if related_name == fk_obj.related_name:
                        template_fk = copy.deepcopy(fk_obj)
                        template_fk.linked_model_name = related_name
                        template_fk.linked_field_name = var_field
                        parent_fk_fields.append(template_fk)
                        break
            else:
                value = row_var_type
                if value:
                    if isinstance(value, Model):
                        fk_field_values.append(GodsownParentLeaf(value, related_name, template_related_name,
                                                                 var_field))
                    else:
                        setattr(template_model, var_field, value)

        return template_model

    def has_dependencies(self, model_name):
        return model_name.lower() in self.dependency.links.keys()

    def get_dependencies(self, model_name):
        return self.dependency.links[model_name.lower()]

    def update_template_data(self):
        for default_var in self.dependency.defaults.keys():
            dep_model_str, dep_field_str = default_var.split(".")
            dep_model = self.convert_str_to_model(dep_model_str)
            raw_value = self.dependency.defaults[default_var]
            if raw_value[0] == '$':
                fixed_value = self._set_special_values(raw_value)
            elif raw_value.isdigit():
                field_class = dep_model._meta.get_field(dep_field_str)
                if "ForeignKey" == field_class.get_internal_type():
                    fixed_value = field_class.rel.to.objects.get(pk=int(raw_value))
                else:
                    fixed_value = int(raw_value)
            else:
                fixed_value = raw_value
            if hasattr(self.model_templates, dep_model_str):
                dep_model_template = getattr(self.model_templates, dep_model_str)
            else:
                dep_model_template = dep_model()
                setattr(self.model_templates, dep_model_str, dep_model_template)

            setattr(dep_model_template, dep_field_str, fixed_value)

    #TODO refactor to make it only output strings
    def get_row_variable(self, variable, model_obj):
            if "." in variable:
                var_name, var_field = variable.split(".")
            else:
                var_name = variable
                var_field = None
            #TODO: change model_obj to $model_obj to prevent naming conflicts
            if var_name == "model_obj":
                base_object = model_obj
                if var_name:
                    if var_field:
                        return getattr(base_object, var_field)
                    else:
                        return base_object
            elif var_name == "$filename":
                return self.file.file_name
            elif "$fk" == var_name[0:3]:
                return RowVariableType.FK_VARIABLE

    def _set_special_values(self, value):
        if ":" in value:
            special_type_str, special_value = value.split(":")
            special_value = special_value.strip()
            special_type_str = special_type_str.lower().strip()
        else:
            special_type_str = value
            special_value = None
        if special_type_str[0] == '$':
            if special_type_str[1:] == 'content_id':
                ct_model = self.convert_str_to_model(special_value)
                return ContentType.objects.get_for_model(ct_model())
            if special_type_str[1:] == 'date':
                month, day, year = map(int, special_value.split("-"))
                return rainbow_format.create_tz_date(month, day, year, settings.TIME_ZONE)
            elif value.lower() == "$true":
                return True
            elif value.lower() == "$false":
                return False
            elif special_type_str[1:] == 'site':
                return Site.objects.get(pk=int(special_value))
            elif special_type_str[1:] == 'user':
                return User.objects.get(username=special_value)

    def get_template(self, model_name):
        try:
            template = copy.deepcopy(getattr(self.model_templates, model_name.lower()))
            return template
        except AttributeError as e:
            logging.error(e)

    def get_fk_foci(self, parent_name, field, parent_foci_dict):
        parent_model = getattr(self.main.fk_field_table, parent_name)

        try:
            fk_tuple = getattr(parent_model, field)
        except AttributeError:
            raise FieldNotInFKFieldsException(field)

        col_indices = parent_foci_dict[field]
        fk_foci_dict = {fk_tuple[1]: col_indices}
        return fk_foci_dict

    def get_fk_related_name(self, parent_related_name, parent_field):
        parent_model = getattr(self.main.fk_field_table, parent_related_name)
        return getattr(parent_model, parent_field.lower())[0]


class SettingsLoader:
    LIST_DELIMITER = ','
    MAIN_RELATED_NAME = "$main"
    setting_file_path = None
    setting_values = None

    def __init__(self, ini_file_path):
        self.setting_file_path = ini_file_path

    #Todo complete saftely define
    def get_settings(self):
        with open(self.setting_file_path, 'r') as ini_file:
            config_values = configparser.RawConfigParser()
            config_values.read_file(ini_file)
            app_list = [app.strip() for app in config_values.get('Settings', 'required_apps').split(self.LIST_DELIMITER)]
            stng_vals = SettingsLibrary(app_list)
            model_str = config_values.get('Settings', 'main_model').strip()
            stng_vals.main.model = stng_vals.convert_str_to_model(model_str)
            stng_vals.main.registered_columns = config_values.get('Settings', 'skipped_columns').split()
            for field_col_pair in config_values.items('ColFields'):
                stng_vals.main.registered_columns += field_col_pair[1].lower().split(',')
                for excel_hdr in field_col_pair[1].split(COLUMN_DELIMITER):
                    stng_vals.main.header_to_field_dict[excel_hdr.lower().strip()] = field_col_pair[0].strip()

            for rn_data_types in config_values.items('RelatedNameDataTypes'):
                self.safely_define(stng_vals.main.related_name_types, rn_data_types[0], rn_data_types[1])
                if self.MAIN_RELATED_NAME not in stng_vals.main.related_name_types.keys():
                    self.safely_define(stng_vals.main.related_name_types, self.MAIN_RELATED_NAME, model_str)
            for row_dep_col_pair in config_values.items('ColRowUniqueDep'):
                model_name, field_name = row_dep_col_pair[0].split(CLASS_FIELD_DELIMITER)
                stng_vals.main.registered_columns += row_dep_col_pair[1].lower().split(',')
                field_row_dict = dict()
                self.safely_define_list(field_row_dict, field_name, row_dep_col_pair[1])
                if model_name in stng_vals.dependency.row_unique_values.keys():
                    stng_vals.dependency.row_unique_values[model_name].update(field_row_dict)
                else:
                    stng_vals.dependency.row_unique_values[model_name] = field_row_dict
            for cell_dep_col_pair in config_values.items('ColCellUniqueDep'):
                stng_vals.main.registered_columns += cell_dep_col_pair[1].lower().split(',')
                model_name, field_name = cell_dep_col_pair[0].split(CLASS_FIELD_DELIMITER)
                field_col_dict = dict()
                self.safely_define_list(field_col_dict, field_name, cell_dep_col_pair[1])
                stng_vals.dependency.cell_unique_values[model_name] = field_col_dict
            for row_vars in config_values.items('DepRowVariables'):
                model_name, field_name = row_vars[0].split(CLASS_FIELD_DELIMITER)
                field_var_dict = dict()
                self.safely_define(field_var_dict, field_name, row_vars[1])
                stng_vals.dependency.row_variables[model_name] = field_var_dict
            for row_vars in config_values.items('RequiredFields'):
                self.safely_define_list(stng_vals.main.required_columns, row_vars[0], row_vars[1])
            #Todo create safely settings here
            for field_frnkey_pair in config_values.items('FKFields'):
                fk_model_str, fk_field_str = field_frnkey_pair[1].split(CLASS_FIELD_DELIMITER)
                parent_model_str, parent_field_str = field_frnkey_pair[0].split(CLASS_FIELD_DELIMITER)
                parent_model_str = self.format(parent_model_str)
                parent_field_str = self.format(parent_field_str)
                if hasattr(stng_vals.main.fk_field_table, parent_model_str):
                    fk_model = getattr(stng_vals.main.fk_field_table, parent_model_str)
                else:
                    fk_model = RelModel()
                    setattr(stng_vals.main.fk_field_table, parent_model_str, fk_model)
                setattr(fk_model, parent_field_str, (fk_model_str.lower(), fk_field_str.lower()))
            for dep_link in config_values.items('DependantLinks'):
                self.safely_define_list(stng_vals.dependency.links, dep_link[0], dep_link[1])
            for dep_qry_fld in config_values.items('DepQuery'):
                self.safely_define_list(stng_vals.dependency.query_fields, dep_qry_fld[0], dep_qry_fld[1])
            stng_vals.main.registered_columns = [col_names.strip() for col_names in stng_vals.main.registered_columns]
            for dependency_var in config_values.items('DependencyDefaults'):
                stng_vals.dependency.defaults[dependency_var[0]] = dependency_var[1]

            try:
                for date_order in config_values.items('DateOrder'):
                    stng_vals.file.date_order[date_order[0]] = self.format(date_order[1])

                for date_format in config_values.items('DateFormat'):
                    stng_vals.file.date_format[date_format[0]] = self.format(date_format[1], case_sensitive=True)
            except configparser.NoSectionError as e:
                logging.warning(e)

            try:
                stng_vals.file.no_year_fields = [self.format(field) for field in
                                                 config_values.get('SheetSpecific', 'no_year').split(",")]
                stng_vals.file.year = config_values.get('SheetSpecific', 'default_year')
            except configparser.NoSectionError as e:
                logging.warning(e)

        return stng_vals

    def safely_define(self, dictionary, key, value):
        dictionary[key.lower().strip()] = self.format(value)

    def safely_define_list(self, dictionary, key, value):
        value_list = value.split(COLUMN_DELIMITER)
        dictionary[self.format(key)] = [self.format(fixed_value) for fixed_value in value_list
                                        if len(self.format(fixed_value)) > 0]

    def safely_append_definition(self, dictionary, key, value):
        if key not in dictionary.keys():
            dictionary[self.format(key)] = []
        dictionary[self.format(key)].append(self.format(value))

    @staticmethod
    def format(value, case_sensitive=False):
        new_value = value
        if isinstance(new_value, str):
            if not case_sensitive:
                new_value = new_value.lower()
            return new_value.strip()
        else:
            return new_value


class GodsownModelFactory:
    global stopper
    model = None
    etl_settings = None
    dependency_loader = None
    template_model = None
    foci_dict = None
    parent = None
    related_name = None
    current_required_fields = None
    field_col_index_dict = None
    initial_settings = None
    #TOdo check if "template" is an instace of "model"

    def __init__(self, related_name, main_model, etl_settings, foci_dict,
                 dependency_loader=None, parent=None, model_template=None):
        self.model = main_model
        self.related_name = related_name
        self.etl_settings = etl_settings
        self.parent = parent
        self.foci_dict = foci_dict
        self.dependency_loader = dependency_loader
        if related_name in self.etl_settings.main.required_columns.keys():
            self.current_required_fields = self.etl_settings.main.required_columns[related_name]
        if model_template:
            self.template_model = model_template
        else:
            self.template_model = self.model()

    #TODO add support for multiple level foreign keys
    def build_model_obj(self, row, field_list=None):
        is_empty = True
        dep = None
        fk_list = []
        if not field_list:
            field_list = self.foci_dict.keys()
        if self.is_required_fields_filled(row):
            if self.template_model:
                new_entry = copy.deepcopy(self.template_model)
                new_entry.id = None
            else:
                new_entry = self.model()
            for field in field_list:
                field_type = new_entry._meta.get_field(field).get_internal_type()

                if field_type == "ForeignKey":
                    fk_model = new_entry._meta.get_field(field).rel.to
                    fk_foci_dict = self.etl_settings.get_fk_foci(self.related_name, field,
                                                                 self.foci_dict)
                    #TODO refactor/merge with SettingsLibarayMethod
                    fk_related_name = self.etl_settings.get_fk_related_name(self.related_name, field)
                    fk_template_mdl = self.etl_settings.get_template(fk_related_name)

                    fk_accessor = GodsownModelFactory(fk_related_name, fk_model, self.etl_settings, fk_foci_dict,
                                                      model_template=fk_template_mdl)
                    fk_obj = fk_accessor.build_model_obj(row)
                    if fk_obj:
                        fk_obj.set_linked_data(self.related_name, field)
                        fk_list.append(fk_obj)
                else:
                    #Todo Replace with get_cell()
                    col_indices = self.foci_dict[field]
                    #TODO check if dict is list

                    if field_type in ["DateTimeField", "DateField"]:
                        if len(col_indices) > 1:
                            # Todo Cant Data Exception(field_type)
                            pass
                        try:
                            default_year = None
                            no_year_fields = self.etl_settings.file.no_year_fields
                            if no_year_fields and field in no_year_fields:
                                default_year = self.etl_settings.file.year
                            full_name = "{}.{}".format(self.related_name, field)

                            if full_name in self.etl_settings.file.date_order.keys():
                                date_order = self.etl_settings.file.date_order[full_name]
                            else:
                                date_order = rainbow_format.DateOrder.Intl

                            if full_name in self.etl_settings.file.date_format.keys():
                                date_format = self.etl_settings.file.date_format[full_name]
                            else:
                                date_format = None

                            value = rainbow_format.convert_to_tz_date(row[col_indices[0]], settings.TIME_ZONE,
                                                                      order=date_order, default_year=default_year,
                                                                      format=date_format)
                        except rainbow_format.InvalidDateFormat as e:
                            logging.error("{}".format(str(e)))
                            value = rainbow_format.create_tz_date(1, 1, 1954, settings.TIME_ZONE)
                    else:
                        value = ""
                        for index in col_indices:
                            value += " " + str(row[index])
                        if field_type == "CharField":
                            field_max_len = new_entry._meta.get_field(field).max_length
                            if len(value) > field_max_len:
                                value = value[:field_max_len-1]
                        value = value.strip()
                    if field_type in ['PositiveIntegerField']:
                        if len(col_indices) > 1:
                            # Todo Cant Concatenate Data Exception(field_type)
                            pass
                        try:
                            value_check = int(value)
                            if value_check < 0:
                                value = None
                        except ValueError:
                            value = None
                    if value:
                        is_empty = False
                        if isinstance(value, tuple):
                            value = value[0]
                        setattr(new_entry, field, value)
            if not is_empty:
                try:
                    new_entry.clean()
                    original_entry = self._find_entry_in_db(new_entry, fk_list)
                    if original_entry:
                        new_entry = original_entry
                except TypeError as e:
                    logging.error(e)

                if self.etl_settings.has_dependencies(self.model.__name__) and self.dependency_loader:
                    dep_list = self.etl_settings.get_dependencies(self.model.__name__)
                    dep = self.dependency_loader.build_dep_models(row, new_entry, dep_list, fk_list)
            else:
                return
        else:
            return
        return GodsownModelBranch(new_entry, self.model, self.related_name, fk_list, dep, self.get_used_fields())

    def is_required_fields_filled(self, row):
        if self.current_required_fields:
            is_all_filled = True
            for req_fld in self.current_required_fields:
                for value in self.get_all_cells(row, req_fld):
                    required_string = str(value).strip()
                    is_all_filled = is_all_filled and len(required_string) > 0
            return is_all_filled
        else:
            return True

    def get_used_fields(self):
        if self.related_name in self.etl_settings.dependency.query_fields.keys():
            used_fields = self.etl_settings.dependency.query_fields[self.related_name]
        else:
            used_fields = list(self.foci_dict.keys())
        return used_fields

    def _find_entry_in_db(self, new_entry, fk_list):
        query_dict = {}
        is_empty = True

        field_list = self.get_used_fields()

        for field in field_list:
            query_dict.setdefault(field, None)
            try:
                field_type = new_entry._meta.get_field(field).get_internal_type()
                if field_type == 'ForeignKey':
                    found_fks = [node.model for node in fk_list if node.linked_field_name == field]
                    if found_fks:
                        fk_model = found_fks[0]
                        if fk_model.pk:
                            query_dict[field] = fk_model.pk
                else:
                    query_dict[field] = getattr(new_entry, field)
                if (isinstance(query_dict[field], str) and len(query_dict[field].strip()) > 0) or query_dict[field]:
                    is_empty = False
            except ObjectDoesNotExist:
                logging.error("{}.{} doesn't exist.".format(self.related_name, field))
        if not is_empty:
            #TODO: Don't return nulls, return custom exception
            try:
                entry_list = self.model.objects.filter(**query_dict)
                result_qty = len(entry_list)

                if len(entry_list) > 1:
                    logging.error("Found {} extra results for finding {} with query, {}. ".format(result_qty-1,
                                                                                                  self.model,
                                                                                                  query_dict))
                if len(entry_list) == 1:
                    return entry_list[0]
            except self.model.DoesNotExist:
                return

    def get_all_cells(self, data, col_name):
        if col_name in self.foci_dict.keys():
            return [data[col_index] for col_index in self.foci_dict[col_name]]
        return []


class DependencyFactory:
    global stopper
    setting_vals = None
    parent_entry = None
    model_str = None

    def __init__(self, etl_settings, model_str):
        self.setting_vals = etl_settings
        self.model_str = model_str.lower()
        self.setting_vals.update_template_data()

    def build_dep_models(self, row, current_entry, dep_list, fk_fields):
        built_dep_models = []
        for rel_model_name in dep_list:
            template_fk_fields_list = []
            try:
                model_type = self.setting_vals.main.related_name_types[rel_model_name]
            except KeyError as e:
                raise RelatedNameHasNoTypeException(str(e).strip("'"))
            dep_model = self.setting_vals.convert_str_to_model(model_type)
            sub_foci_dict = self.setting_vals.dependency.ru_doci_dict[self.model_str][rel_model_name]
            if rel_model_name in self.setting_vals.dependency.row_variables:
                model_tmplte = self.setting_vals.create_template_model(rel_model_name,
                                                                       parent_entry=self.parent_entry,
                                                                       current_entry=current_entry,
                                                                       fk_field_values=template_fk_fields_list,
                                                                       parent_fk_fields=fk_fields)
            else:
                model_tmplte = dep_model()

            new_dep_loader = DependencyFactory(self.setting_vals, rel_model_name)
            cu_indices = [None]
            cu_field = None
            cu_model_foci_dict = self.setting_vals.dependency.cu_doci_dict[self.model_str]
            fk_table = self.setting_vals.main.fk_field_table
            fk_fields = set()
            if hasattr(fk_table, rel_model_name):
                fk_foci_fields = getattr(fk_table, rel_model_name).__dict__.keys()
                fk_fields |= set(sub_foci_dict.keys())
                fk_fields |= set(fk_foci_fields)
            if rel_model_name in cu_model_foci_dict.keys():
                #Todo figure out implementation for multiple cu cells for one model
                model_cu_foci_dict = cu_model_foci_dict[rel_model_name]
                cu_field = next(iter(model_cu_foci_dict))
                fk_fields.add(cu_field)
                cu_indices = model_cu_foci_dict[cu_field]
            for cu_indx in cu_indices:
                if isinstance(cu_indx, int):
                    cu_dict = {cu_field: [cu_indx]}
                    final_foci_dict = dict(sub_foci_dict, **cu_dict)
                else:
                    final_foci_dict = sub_foci_dict
                dep_model_loader = GodsownModelFactory(rel_model_name, dep_model, self.setting_vals,
                                                       foci_dict=final_foci_dict,
                                                       model_template=model_tmplte,
                                                       dependency_loader=new_dep_loader)
                dep_obj = dep_model_loader.build_model_obj(row)
                if dep_obj:
                    found_object = get_model_from_db(dep_obj, template_fk_fields_list)
                    if not found_object:
                        dep_obj.fk += template_fk_fields_list
                        built_dep_models.append(dep_obj)
        return built_dep_models


class GodsownParser:
    initial_settings = None

    def __init__(self, setting_file_path):
        self.model_modules_used = None
        self.template_model_values = []
        self.entry_list = []
        self.field_col_index_dict = dict()
        setting_ldr = SettingsLoader(setting_file_path)
        self.initial_settings = setting_ldr.get_settings()
        self.initial_settings.update_template_data()
        self.dependency_loader = DependencyFactory(self.initial_settings, self.initial_settings.main.model.__name__)

    def init_header_data(self, header_data, file_origin):
        self.field_col_index_dict = dict()
        ru_doci_dict = dict()
        cu_doci_dict = dict()
        self.initial_settings.file.file_name = file_origin
        for i, header in enumerate(header_data):
            self._init_field_row(i, header)

        for model_name in self.initial_settings.dependency.links.keys():
            ru_model_dict = ru_doci_dict[model_name] = dict()
            cu_model_dict = cu_doci_dict[model_name] = dict()
            for dep_model_name in self.initial_settings.dependency.links[model_name]:
                ru_model_dict[dep_model_name.lower()] = dict()
                cu_model_dict[dep_model_name.lower()] = dict()
            for x, header in enumerate(header_data):
                    try:
                        ru_model_dict = self._build_foci(self.initial_settings.dependency.row_unique_values, ru_model_dict,
                                                         x, header)
                    except KeyError as e:
                        logging.error("Data from column [{}] is a Foreign Key but not a Row ".format(header) +
                                      "Unique Dep. No dependants for {} data type".format(str(e)))
                    try:
                        cu_model_dict = self._build_foci(self.initial_settings.dependency.cell_unique_values, cu_model_dict,
                                                         x, header)
                    except KeyError as e:
                        logging.error("Data from column [{}] is a Foreign Key but not a Cell Unique".format(header) +
                                      " Dep. No dependants for {} data type".format(str(e)))
            empty_dict_list = []
            for cu_model_name in cu_model_dict.keys():
                if len(cu_model_dict[cu_model_name].keys()) <= 0:
                    empty_dict_list.append(cu_model_name)

            for empty_model_name in empty_dict_list:
                cu_model_dict.pop(empty_model_name)

        self.initial_settings.dependency.ru_doci_dict = ru_doci_dict
        self.initial_settings.dependency.cu_doci_dict = cu_doci_dict

    def _init_field_row(self, index, col_name):
        fixed_col_name = col_name.lower().strip()
        try:
            field_name = self.initial_settings.main.header_to_field_dict[fixed_col_name].lower()
            if not self.field_col_index_dict.get(field_name, None):
                self.field_col_index_dict[field_name] = []
            self.field_col_index_dict[field_name].append(index)
            return True
        except KeyError:
            return False

    def _build_foci(self, excel_value_dict, output_foci, index, col_name):
        for model_field in excel_value_dict.items():
            model_name, field_dict = model_field
            for field_cols in field_dict.items():
                field_name, col_list = field_cols
                if col_name in col_list:
                    field_index_dict = output_foci[model_name]
                    if not field_index_dict.get(field_name, None):
                        field_index_dict[field_name] = []
                    field_index_dict[field_name].append(index)
        return output_foci


#TODO Find Comment Methods with TextFodsownParser and this to add to GodsownParser
class ExcelGodsownParser(GodsownParser):
    required_columns = None
    main_column_name = None
    initial_settings = None
    field_col_index_dict = None
    #TODO test if header_dict can be removed

    def init_file(self, sheet, first_header_row, col_range=1, file_name=None):
        self.field_col_index_dict = dict()
        self.initial_settings.file.file_name = file_name
        row_size = len(sheet.row_values(first_header_row))
        for x in range(row_size):
            header_pieces = []
            for y in range(first_header_row, first_header_row+col_range):
                header_pieces.append(str(sheet.cell(y, x).value).strip())
            header_name = " ".join(header_pieces).lower().strip()
            self._init_field_row(x, header_name)

    def _init_field_row(self, index, col_name):
        try:
            field_name = self.initial_settings.main.header_to_field_dict[col_name]
            if not self.field_col_index_dict.get(field_name, None):
                self.field_col_index_dict[field_name] = []
            self.field_col_index_dict[field_name].append(index)
            return True
        except KeyError:
            return False

    def load_row(self, row):
        initial_accessor = GodsownModelFactory(SettingsLoader.MAIN_RELATED_NAME, self.initial_settings.main.model,
                                               self.initial_settings, self.field_col_index_dict,
                                               dependency_loader=self.dependency_loader,
                                               model_template=self.initial_settings.create_template_model("main_model"))
        initial_accessor.build_model_obj(row)


class TextGodsownParser(GodsownParser):
    row_delimiter = "\r\n"
    cell_delimiter = "\t"
    row_size = None
    ini_file_path = None
    excel_type = None
    data = None
    accessor = None

    def __init__(self, excel_file_location, data, header_data, origin=None):
        if not origin:
            origin = self.__class__.__name__
        super().__init__(excel_file_location)
        self.data = data
        self.init_header_data(header_data, origin)
        self.row_size = len(header_data)
        template = self.initial_settings.create_template_model(SettingsLoader.MAIN_RELATED_NAME)
        self.accessor = GodsownModelFactory(SettingsLoader.MAIN_RELATED_NAME, self.initial_settings.main.model,
                                            self.initial_settings, self.field_col_index_dict,
                                            dependency_loader=self.dependency_loader,
                                            model_template=template)

    def create_data(self):
        output_data = []
        row_list = self.data.split(self.row_delimiter)
        for row in row_list:
            cells = row.split(self.cell_delimiter)

            if len(cells) >= self.row_size:
                logging.warning("Rows given by the user are longer than the headers given. Truncating cells from {}" +
                                " to {}".format(len(cells), self.row_size))
                cells = cells[:self.row_size]
                model = self.accessor.build_model_obj(cells)
                output_data.append(model)
            else:
                logging.error("Invalid Data Given. Rows is too short. Skipping row: {}".format(row))
        return output_data


class GodsownDataLoader:
    def save(self, data):
        raise NotImplementedError


class DjangoORMLoader(GodsownDataLoader):
    def save(self, data):
        self._save_by_hierarchy(data, None)

    def _save_by_hierarchy(self, data_node, parent):
        if isinstance(data_node, GodsownModelBranch):
            db_model = get_model_from_db(data_node)
            if db_model:
                data_node.model.pk = db_model.pk
            for foreign_key_branch in data_node.fk:
                self._save_by_hierarchy(foreign_key_branch, data_node)
                setattr(data_node.model, foreign_key_branch.linked_field_name, foreign_key_branch.model)
            if not data_node.model.pk:
                self._save_data(data_node)
            else:
                logging.warning(data_node.model, "is already in database")

            if data_node.dependencies:
                for dep in data_node.dependencies:
                    self._save_by_hierarchy(dep, data_node)
        elif isinstance(data_node, GodsownParentLeaf):
            setattr(parent.model, data_node.linked_field_name, data_node.model)
        else:
            logging.error("Found unknown type in object {}: {}".format(type(data_node), data_node))

    def _save_data(self, data):
        #TODO: Once models are completely isolated from ModelFactory, contruct model here
        data.model.save()


def get_model_from_db(model_node, fk_node_list=None):
    query_dict = {}
    is_not_empty = True

    field_list = model_node.used_fields
    for field in field_list:
        query_dict.setdefault(field, None)
        try:
            field_type = model_node.model._meta.get_field(field).get_internal_type()
            if field_type == 'ForeignKey':
                if fk_node_list:
                    matching_model = [node.model for node in fk_node_list
                                      if field == node.linked_field_name]
                else:
                    matching_model = []

                matching_model += [node.model for node in model_node.fk
                                   if field == node.linked_field_name]
                if len(matching_model) == 1:
                    query_dict[field] = matching_model[0].pk
                elif len(matching_model) > 1:
                    logging.error("Multiple objects with the same related_names")
            else:
                query_dict[field] = getattr(model_node.model, field)
            is_not_empty = (isinstance(query_dict.get(field, None), str) and len(query_dict[field].strip()) > 0) \
                or field in query_dict.keys()
        except ObjectDoesNotExist:
            logging.error("{}.{} doesn't exist.".format(model_node.related_name, field))
    if is_not_empty and query_dict:
        # TODO: Don't return nulls, return custom exception
        entry_list = model_node.model_type.objects.filter(**query_dict)
        if len(entry_list) > 1:
            logging.error("Found {} extra results for finding {} with query, {}. ".format(len(entry_list) - 1,
                                                                                          model_node.model,
                                                                                          query_dict))
        if len(entry_list) > 0:
            return entry_list[0]
    return


#Turn into settings manager with parameters (excel_type, header_type)
class QuickImportUtils:
    @staticmethod
    def get_excel_type_names():
        config = configparser.RawConfigParser()
        xl_type_settings = os.listdir(EXCEL_TYPE_FOLDER)
        name_list = []

        for xl_type_file in xl_type_settings:
            with open(os.path.join(EXCEL_TYPE_FOLDER, xl_type_file), 'r') as excel_type_file:
                config.read_file(excel_type_file)
                name_list.append(config.get("Settings", "name"))
        return name_list

    @staticmethod
    def get_xlsttg_filepath(excel_type_name):
        config = configparser.RawConfigParser()
        xl_type_settings = os.listdir(EXCEL_TYPE_FOLDER)

        for xl_type_file in xl_type_settings:
            file_dir = os.path.join(EXCEL_TYPE_FOLDER, xl_type_file)
            with open(file_dir, 'r') as excel_type_file:
                config.read_file(excel_type_file)
                if excel_type_name.strip() == config.get("Settings", "name").strip():
                    return file_dir

    @staticmethod
    def get_header_data(header_filename):
        with open(os.path.join(HEADER_NAMES_FOLDER, header_filename), 'r') as header_file:
            raw_header_rows = header_file.readlines()
            header_rows = [row.split(TextGodsownParser.cell_delimiter) for row in raw_header_rows]
            final_header_row = header_rows[0]
            for row in header_rows[1:]:
                final_header_row = ["{} {}".format(final_header_row[i], row[i]).lower().strip()
                                    for i in range(len(row))]
            header_file.close()

        return final_header_row
