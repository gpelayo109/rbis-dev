from django.contrib.auth.models import User
from rainbow_remarks import Remark
from datetime import timedelta, datetime
import pytz

MNL_TZ = pytz.timezone("Asia/Manila")
MAX_DAYS = 14
DEFAULT_DAYS = 7


def get_data_args(request):
    class RemarkData:
        name = None
        remark_record = None

        def __init__(self):
            self.remark_record = []
            self.name = ""

    start_date_str = request.POST.get('start', None)
    end_date_str = request.POST.get('end', None)
    day_offset = DEFAULT_DAYS
    error_msgs = None

    if start_date_str:
        naive_start_day = datetime.strptime(start_date_str, "%m/%d/%Y")
    else:
        naive_start_day = datetime.today()

    start_day = MNL_TZ.localize(naive_start_day)

    if end_date_str:
        end_day = MNL_TZ.localize(datetime.strptime(end_date_str, "%m/%d/%Y"))
        offset_diff = (end_day - start_day).days + 1
        if start_date_str:
            if offset_diff < 0:
                error_msgs = "Sorry. You gave an invalid input. You START DATE must be earlier than your END DATE."\
                    .format(offset_diff, MAX_DAYS)
                day_offset = 0
            elif offset_diff < MAX_DAYS:
                day_offset = offset_diff
            else:
                error_msgs = "Sorry. You can't select a date range of {} days. Must be {} days or less."\
                    .format(offset_diff, MAX_DAYS)
                day_offset = DEFAULT_DAYS
        else:
            start_day = end_day - timedelta(days=DEFAULT_DAYS)
    raw_date_list = [start_day + timedelta(days=offset) for offset in range(day_offset)]

    user_list = []

    for user in User.objects.all():
        remark_data = RemarkData()
        remark_data.name = user.username
        for bgn_hour in raw_date_list:
                end_hour = bgn_hour + timedelta(days=1)
                count = Remark.objects.filter(user=user.pk, is_removed=False,
                                              date_posted__gte=bgn_hour,
                                              date_posted__lte=end_hour).count()
                remark_data.remark_record.append(count)
        user_list.append(remark_data)

    date_list = [date.strftime("%m-%d-%y") for date in raw_date_list]

    data_args = {
        #Contains String that will display in the Larger Header
        'title': "Remark Statistics",
        #Contains Value to determine user has signed in. Enables user-tools (login, logout, etc)
        'has_permission': request.user.is_authenticated(),
        'can_view': request.user.is_superuser,
        'date_list': date_list,
        'user_list': user_list,
        'errors': error_msgs,
    }

    return data_args
