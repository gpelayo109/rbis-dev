from django import forms
from django.contrib import admin
from django.contrib.admin.filters import *
from django.contrib.admin.widgets import AdminDateWidget, AdminSplitDateTime
from django.utils.html import format_html
from django.utils.translation import ugettext as _
from django.contrib.admin.templatetags.admin_static import static
from rainbow_db import settings
from data.models import *
from rainbow_remarks.models import *
import datetime


BASE_DIR = settings.BASE_DIR


class SimpleDropDownFilter(SimpleListFilter):
    template = os.path.join(BASE_DIR, 'templates', 'rainbow', 'dropdownfilter.html')


class RelatedFieldDropDownFilter(RelatedFieldListFilter):
    template = os.path.join(BASE_DIR, 'templates', 'rainbow', 'dropdownfilter.html')


class ClientsDealersFilter(SimpleDropDownFilter):
    title = 'Dealers'
    parameter_name = 'associate'

    def lookups(self, request, model_admin):
        return [(associate.pk, str(associate)) for associate in Associate.objects.all().order_by('first_name')]

    def queryset(self, request, queryset):
        if self.value():
            print("[ClientDealerFilter] Value:\t\t%s" % self.value())
            pairs = DealerClientPair.objects.filter(dealer=self.value())
            # print("[ClientDealerFilter] Pairs:\t\t%s" % pairs)
            client_pks = []
            for pair in pairs:
                if "" == str(pair.client).strip():
                    print("[ClientDealerFilter] Num:\t\t%s" % str(pair.client.contact_number))
                    print("[ClientDealerFilter] Client:\t\t%s" % str(pair.client))
                    print("[ClientDealerFilter] Dealer:\t\t%s" % str(pair.dealer))
                client_pks.append(pair.client.pk)
            return queryset.filter(pk__in=client_pks)
        else:
            return queryset


class RemarkTypeFilter(SimpleDropDownFilter):
    title = 'Latest Remark Type'

    parameter_name = 'remark_type'

    def lookups(self, request, model_admin):
        return [(remark_type.pk, remark_type.abbreviation) for remark_type in RemarkType.objects.all().order_by('pk')]

    def queryset(self, request, queryset):
        if self.value():
            client_pks = [remark.object_pk for remark in Remark.objects.filter(type=self.value())]
            print("[RemarkType] Value:\t\t%s" % client_pks)
            return queryset.filter(pk__in=client_pks)
        else:
            return queryset


class FileOriginFilter(AllValuesFieldListFilter):
    title = 'Original Excel File'
    template = os.path.join(BASE_DIR, 'templates', 'rainbow', 'dropdownfilter.html')


class CityFilter(SimpleDropDownFilter):
    title = 'City/Area'
    parameter_name = 'city'

    def lookups(self, request, model_admin):
        return [(city.pk, city.name)for city in City.objects.order_by('name')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(city=self.value())
        else:
            return queryset


class DealerFilter(RelatedFieldDropDownFilter):
    title = 'Dealer'

    parameter_name = 'associate'

    def field_choices(self, field, request, model_admin):
        qs = [(associate.pk, str(associate)) for associate in Associate.objects.order_by('first_name')]
        return qs


class OriginalAsFilter(SimpleDropDownFilter):
    title = 'Original AS'

    parameter_name = 'original_as'

    def lookups(self, request, model_admin):
        return [(associate.pk, str(associate)) for associate in Associate.objects.all().order_by('first_name')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(original_as=self.value())
        else:
            return queryset


class DateRangeFilterAdminSplitDateTime(AdminSplitDateTime):
    def format_output(self, rendered_widgets):
        return format_html('<p>{0} {1}<br />{2} {3}</p>',
                           '', rendered_widgets[0],
                           '', rendered_widgets[1])


class DateRangeFilterBaseForm(forms.Form):
    def __init__(self, request, *args, **kwargs):
        super(DateRangeFilterBaseForm, self).__init__(*args, **kwargs)
        self.request = request

    @property
    def media(self):
        try:
            if getattr(self.request, 'daterange_filter_media_included'):
                return forms.Media()
        except AttributeError:
            setattr(self.request, 'daterange_filter_media_included', True)

            js = ["calendar.js", os.path.join("admin", "DateTimeShortcuts.js")]
            css = ['widgets.css']

            return forms.Media(
                js=[static(os.path.join("admin", "js", path) for path in js)],
                css={'all': [static(os.path.join("admin", "css", path)) for path in css]}
            )


class DateRangeForm(DateRangeFilterBaseForm):
    def __init__(self, *args, **kwargs):
        field_name = kwargs.pop('field_name')
        super(DateRangeForm, self).__init__(*args, **kwargs)

        self.fields['%s__gte' % field_name] = forms.DateField(
            label='',
            widget=AdminDateWidget(
                attrs={'placeholder': _('From date')}
            ),
            localize=True,
            required=False
        )

        self.fields['%s__lte' % field_name] = forms.DateField(
            label='',
            widget=AdminDateWidget(
                attrs={'placeholder': _('To date')}
            ),
            localize=True,
            required=False,
        )


class DateRangeFilter(admin.filters.FieldListFilter):
    template = os.path.join(BASE_DIR, 'templates','rainbow', 'daterangefilter.html')

    def __init__(self, field, request, params, model, model_admin, field_path):
        self.lookup_kwarg_since = '%s__gte' % field_path
        self.lookup_kwarg_upto = '%s__lte' % field_path
        super(DateRangeFilter, self).__init__(
            field, request, params, model, model_admin, field_path)
        self.form = self.get_form(request)

    def choices(self, cl):
        return []

    def expected_parameters(self):
        return [self.lookup_kwarg_since, self.lookup_kwarg_upto]

    def get_form(self, request):
        return DateRangeForm(request, data=self.used_parameters,
                             field_name=self.field_path)

    def queryset(self, request, queryset):
        if self.form.is_valid():
            # get no null params
            filter_params = dict(filter(lambda x: bool(x[1]),
                                        self.form.cleaned_data.items()))

            # filter by upto included
            if filter_params.get(self.lookup_kwarg_upto) is not None:
                lookup_kwarg_upto_value = filter_params.pop(self.lookup_kwarg_upto)
                filter_params['%s__lt' % self.field_path] = lookup_kwarg_upto_value + datetime.timedelta(days=1)

            return queryset.filter(**filter_params)
        else:
            return queryset

# register the filters
admin.filters.FieldListFilter.register(lambda f: isinstance(f, models.DateField), DateRangeFilter)