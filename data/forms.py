from django.forms.models import BaseInlineFormSet


class RequiredInlineFormset(BaseInlineFormSet):
    """
    An Inline Formset for fields requiring at least one item
    """

    def _construct_form(self, i, **kwargs):
        form = super(RequiredInlineFormset, self)._construct_form(i, **kwargs)
        form.empty_permitted = False
        return form
