from datetime import datetime, timedelta
from django.conf import settings as django_settings
from django.contrib import auth
from rainbow_db import version

__author__ = 'Geryl'


class ScheduleLogout:
    def process_request(self, request):
        print('[ScheduleMiddleware] Processing')
        if not request.user.is_authenticated():
            print('[ScheduleLogout] User not authenticated')
            return

        if 'login_time' not in request.session.keys():
            request.session['login_time'] = str(datetime.now())
        if 'last_touch' not in request.session.keys():
            request.session['last_touch'] = str(datetime.now())

        login_time = datetime.strptime(request.session['login_time'], "%Y-%m-%d %H:%M:%S.%f")
        last_touch = datetime.strptime(request.session['last_touch'], "%Y-%m-%d %H:%M:%S.%f")

        current_time = int(datetime.strftime(last_touch, "%H%M"))
        login_time_int = int(datetime.strftime(login_time, "%H%M"))
        auto_logout_time = django_settings.AUTO_LOGOUT_IDLE
        if datetime.now() - last_touch > timedelta(minutes=auto_logout_time) and auto_logout_time != -1:
            print('[ScheduleLogout] Logged out after idle for ' + str(auto_logout_time) + ' minutes.')
            self.logout(request)
            return
        elif login_time_int < django_settings.SCHEDULED_LOG0UT_TIME < current_time:
            print('[ScheduleLogout] Logged out after time ' + str(current_time))

        request.session['last_touch'] = str(datetime.now())

    @staticmethod
    def logout(request):
        del request.session['last_touch']
        del request.session['login_time']
        auth.logout(request)
        request.session['warning'] = "You have been logged out"


class Stopwatch:
    # DEBUG_DATA_FOLDER = os.path.join(settings.BASE_DIR, "debug-data")
    # DEBUG_FILE_PATH = os.path.join(DEBUG_DATA_FOLDER, "loading-time-data.txt")
    IGNORE_PATHS = ['/jsi18n/']

    start_time = datetime.now()
    # debug_file = ""

    def __init__(self):
        super(Stopwatch, self).__init__()

        # if not os.path.isdir(self.DEBUG_DATA_FOLDER):
        #     os.mkdir(self.DEBUG_DATA_FOLDER)

        # self.debug_file = open(self.DEBUG_FILE_PATH, "a+")

    def process_request(self, request):
        # if self.debug_file.closed:
            # self.debug_file = open(self.DEBUG_FILE_PATH, "a+")

        self.start_time = datetime.now()

    def process_response(self, request, response):
        # if self.debug_file.closed:
        #     self.debug_file = open(self.DEBUG_FILE_PATH, "a+")

        if request.path not in self.IGNORE_PATHS:
            version_code = version.get_version_code()
            time_loaded = (datetime.now() - self.start_time).total_seconds()
            date = datetime.now().strftime("%y%m%d-%H:%M:%S")
            # debug_data = "{}\t{}\t{:.6f}\t{}\n".format(date, version_code, time_loaded, request.path)
            # self.debug_file.write(debug_data)
            # self.debug_file.close()
        return response

    def process_exception(self, request, exception):
        pass
        # self.debug_file.close()
