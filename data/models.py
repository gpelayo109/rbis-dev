from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from django.utils import timezone
import operator
import os
from functools import reduce
from rainbow_remarks.models import Remark

STATIC_FOLDER = os.path.join("..", "static", "admin")
DEFAULT_BLANK_VALUE = ''

#
#  Permissions
#
VIEW_DETAILS = "view_details"
ADMIN_BASE_EDIT = "admin_edit"
ADMIN_INLINE_EDIT = "admin_inline"


class City(models.Model):
    name = models.CharField(max_length=35, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "cities"
        ordering = ["name"]


class ClientConcern(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class AssociatePosition(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Position"


def associate_photo_filename(instance, filename):
    return os.path.join('associate_photo', "-".join([instance.first_name, instance.last_name, str(instance.pk)]))


class Associate(models.Model):
    # associate_photo = models.ImageField(upload_to=associate_photo_filename, null=True, blank=True)
    position = models.ForeignKey(AssociatePosition, db_index=True)
    database_user_name = models.OneToOneField(User, null=True, blank=True)
    date_started = models.DateField(null=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birthday = models.DateField(null=True)
    unit_no = models.CharField(max_length=5, null=True, blank=True)
    building = models.CharField(max_length=35, null=True, blank=True)
    street = models.CharField(max_length=35, null=True, blank=True)
    city = models.ForeignKey(City, null=True, blank=True, db_index=True)
    contact_number = models.CharField(max_length=12, null=True, blank=True)
    email = models.CharField(max_length=35, null=True, blank=True)
    facebook = models.CharField(max_length=35, blank=True, null=True)
    twitter = models.CharField(max_length=35,  blank=True, null=True)
    is_active = models.BooleanField()

    field_order = [#'preview', 'associate_photo',
                   'first_name', 'last_name',
                   'position', 'database_user_name',
                   'birthday', 'date_started',
                   'building', 'contact_number',
                   'street', 'email',
                   'city', 'facebook',
                   'is_active', 'twitter']

    def preview(self):
        return '<img height=100 width=100 src="%s" />' % self.associate_photo.url
    preview.short_description = ''
    preview.allow_tags = True

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name).strip()

    class Meta:
        ordering = ["first_name", "last_name"]


class Client(models.Model):
    first_name = models.CharField(max_length=40, null=True, blank=True, db_index=True)
    last_name = models.CharField(max_length=40, null=True, blank=True)
    profession = models.CharField(max_length=50, null=True, blank=True)
    unit_no = models.CharField(max_length=10, null=True, blank=True)
    building = models.CharField(max_length=100, null=True, blank=True)
    street = models.CharField(max_length=210, null=True, blank=True)
    city = models.ForeignKey(City, null=True, blank=True, verbose_name="City/Area")
    is_banned = models.BooleanField(default=False)
    is_a_rainbow_owner = models.BooleanField(default=False)
    basic_unit = models.CharField(max_length=8, null=True, blank=True)
    power_nozzle = models.CharField(max_length=8, null=True, blank=True)
    email = models.CharField(max_length=50, null=True, blank=True)
    best_time = models.CharField(verbose_name="Best Time to Call", null=True, blank=True, max_length=35)
    MARITAL_STATUS_CHOICES = (('m', "Married"), ('s', "Single"))
    marital_status = models.CharField(max_length=50, null=True, blank=True, choices=MARITAL_STATUS_CHOICES)
    client_concerns = models.ManyToManyField(ClientConcern, blank=True)
    other_concerns = models.CharField(max_length=500, null=True, blank=True)
    original_as = models.ForeignKey(Associate, db_index=True, verbose_name='Original A.S.', null=True, blank=True)
    file_origin = models.CharField(max_length=255, null=True, blank=True)

    field_order = ['first_name', 'best_time',
                   'last_name', 'marital_status',
                   'unit_no', 'profession',
                   'building', 'email',
                   'street', 'is_a_rainbow_owner',
                   'city', 'basic_unit',
                   'client_concerns',  'power_nozzle',
                   'other_concerns',  'original_as',
                   'is_banned']

    displayed_fields = ['first_name', 'last_name', 'get_phone_numbers', 'get_dealers', 'get_referrals',
                        'city', 'original_as', 'get_latest_remark']
    search_fields = ['last_name', 'first_name', "PhoneNumber.number"]

    def get_referrals(self):
        return ','.join([str(referral.referred_by) for referral in Referral.objects.filter(client=self.id)])
    get_referrals.short_description = 'Referred By'

    def get_dealers(self):
        pairs = DealerClientPair.objects.filter(client=self.pk)
        if pairs:
            dealer_query = reduce(operator.or_, [Q(pk=pair.dealer.pk) for pair in pairs])
            client_list = Associate.objects.filter(dealer_query)
        else:
            client_list = []

        return ", ".join([str(client) for client in client_list])
    get_dealers.short_description = "Dealers"

    def get_phone_numbers(self):
        phone_numbers_list = PhoneNumber.objects.filter(client=self.pk)
        return "; ".join([str(client) for client in phone_numbers_list])
    get_phone_numbers.short_description = "Contact Number(s)"

    def get_latest_remark(self):
        remark = Remark.get_latest_remark(self)
        if remark.is_empty():
            return ""
        else:
            remark_type = remark.type
            if not remark_type:
                remark_type_str = ""
            else:
                remark_type_str = "[%s]" % remark_type.abbreviation

            return "{}: {} {}".format(remark.date_posted.strftime('%Y-%m-%d'),
                                         remark_type_str, remark.comment)
    get_latest_remark.short_description = "Latest Remark"

    def get_city_name(self):
        return str(self.city.name)
    get_city_name.short_description = 'City Name'

    def get_full_address(self):
        full_address = ""
        if self.unit_no is not None and self.street is not "":
            full_address += self.unit_no + " "
        if self.building is not None and self.street is not "":
            full_address += self.building + " "
        if self.street is not None and self.street is not "":
            full_address = self.add_comma_if_ok(full_address)
            full_address += self.street + " "
        if self.city is not None and self.street is not "":
            full_address = self.add_comma_if_ok(full_address)
            full_address += self.city + " "
        if self.zip_code is not None and self.street is not "":
            full_address = self.add_comma_if_ok(full_address)
            full_address += str(self.zip_code) + " "
        return full_address
    get_full_address.short_description = 'Address'

    def __getattribute__(self, name):
        attr = models.Model.__getattribute__(self, name)
        list_view_fields = ['city', 'referred_by', 'contact_number']
        if name in list_view_fields and not attr:
            return DEFAULT_BLANK_VALUE
        return attr

    def __str__(self):
        final_string = ""
        if self.first_name:
            final_string = self.first_name

        if self.last_name and self.first_name:
            final_string += " "

        if self.last_name:
            final_string += self.last_name
        return final_string

    @staticmethod
    def add_comma_if_ok(full_address):
        current_len = len(full_address)
        if current_len > 0:
            full_address = full_address[:-1] + ", "
        return full_address

    @staticmethod
    def large_icon_url():
            return STATIC_FOLDER + "img/Client.png"

    class Meta:
        permissions = (
            (VIEW_DETAILS, "Can view client info"),
            (ADMIN_BASE_EDIT, "Can edit base client data"),
            (ADMIN_INLINE_EDIT, "Can edit inline client data"),
        )
        ordering = ["first_name", "last_name"]


class PhoneNumber(models.Model):
    NUMBER_MAX_LENGTH = 35
    number = models.CharField(max_length=NUMBER_MAX_LENGTH, unique=True)
    client = models.ForeignKey(Client, blank=True, null=True)

    def __str__(self):
        return "{}".format(self.number)

    #TODO remove once delimiter split option is implmeneted
    def clean(self, *args, **kwargs):
        if "." in self.number:
            defloated_num = self.number.split(".")
            if defloated_num[0]:
                self.number = defloated_num[0]
            else:
                self.number = defloated_num[1]
        elif "/" in self.number:
            number_list = self.number.split("/")
            if len(number_list) > 1:
                self.number = number_list[0]
                for number in number_list[1:]:
                    new_number = PhoneNumber()
                    new_number.number = number
                    new_number.client = self.client
                    new_number.clean()
                    new_number.save()

        if self.number:
            if '0' == self.number[0]:
                self.number = self.number[1:]

        digit_number = ""
        for digit in self.number:
            if digit.isdigit():
                digit_number += digit
        super(PhoneNumber, self).clean()


class DealerClientPair(models.Model):
    date_assigned = models.DateField(default=timezone.now)
    dealer = models.ForeignKey(Associate, db_index=True)
    client = models.ForeignKey(Client, db_index=True)

    class Meta:
        verbose_name = "Client's Dealer"
        verbose_name_plural = "Client's Dealers"


class Referral(models.Model):
    date_created = models.DateField(default=timezone.now)
    client = models.ForeignKey(Client, related_name='referred_client', db_index=True)
    referred_by = models.ForeignKey(Client, related_name='referrer', blank=True, null=True, db_index=True)
    relationship = models.CharField(max_length=35, blank=True, null=True)

    def __str__(self):
        return "{} on {}".format(self.referred_by, self.date_created.strftime('%Y-%m-%d'))


class ServiceType(models.Model):
    name = models.CharField(max_length=10)
    description = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Program(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class ServiceStatus(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "service statuses"


class Vehicle(models.Model):
    name = models.CharField(max_length=35)

    def __str__(self):
        return self.name


class ServiceAppointment(models.Model):
    date_created = models.DateField(default=timezone.now, verbose_name="Date Set")
    client = models.ForeignKey(Client, related_name='client', db_index=True)
    demo_date = models.DateField()
    demo_time = models.CharField(max_length=35, null=True, blank=True)
    created_by = models.ForeignKey(Associate, null=True, blank=True, db_index=True, verbose_name="Appointment Setter")
    call_in = models.CharField(null=True, blank=True, max_length=35)
    call_out = models.CharField(null=True, blank=True, max_length=35)
    leads_given = models.PositiveIntegerField(null=True, blank=True)
    will_follow_up = models.BooleanField(default=False)
    program_joined = models.ForeignKey(Program, null=True, blank=True, db_index=True)
    demo_done_by = models.ForeignKey(Associate, related_name='demo_done_by', null=True, blank=True, db_index=True)
    assistant = models.ForeignKey(Associate, related_name='assistant', null=True, blank=True, db_index=True)
    service_status = models.ForeignKey(ServiceStatus, null=True, blank=True, db_index=True)
    vehicle_used = models.ForeignKey(Vehicle, null=True, blank=True, db_index=True)
    service_type = models.ForeignKey(ServiceType, null=True, blank=True, db_index=True)

    field_order = ['client', 'call_in',
                   'created_by', 'call_out',
                   'demo_date', 'leads_given',
                   'service_type', 'demo_done_by',
                   'program_joined', 'assistant',
                   'service_status', 'will_follow_up']

    def get_latest_date_referred(self):
        referral_list = Referral.objects.filter(client=self.client.pk).order_by('-date_created')
        if len(referral_list) > 0:
            return referral_list[0].date_created
        else:
            return DEFAULT_BLANK_VALUE
    get_latest_date_referred.short_description = "Latest Date Referred"

    def get_latest_comment(self):
            return Remark.get_latest_remark(self).comment

    def get_client_dealers(self):
            return self.client.get_dealers()
    get_client_dealers.short_description = "Dealer"
    get_client_dealers.admin_order_field = "client__dealer"

    def get_city(self):
        return self.client.city
    get_city.short_description = "City"
    get_city.admin_order_field = "client__city"

    def __getattribute__(self, name):
        attr = models.Model.__getattribute__(self, name)
        list_view_fields = ['created_by', 'demo_done_by', 'service_type', 'service_status', 'city']
        if name in list_view_fields and not attr:
            return DEFAULT_BLANK_VALUE
        return attr

    def __str__(self):
        date = self.demo_date.strftime('%m-%d-%y')
        try:
            client = str(self.client)
        except Client.DoesNotExist:
            client = "Pending"

        return "[{}] on [{}]".format(date, client)

    class Meta:
        verbose_name_plural = "Service Appointments"
        permissions = (
            (VIEW_DETAILS, "Can view service appointment info"),
            (ADMIN_BASE_EDIT, "Can admin service appointment edit "),
        )


class UserLoginHistory(models.Model):
    user = models.ForeignKey(User, db_index=True)
    login_time = models.DateTimeField()

    def __str__(self):
        return str(self.login_time.strftime('%Y-%m-%d %H:%M')) \
            + " - " + str(self.user)

    class Meta:
        verbose_name_plural = "User Login Histories"


def call_on_login(instance, **kwargs):
    if instance.last_login:
        old = instance.__class__.objects.get(pk=instance.pk)
        if instance.last_login != old.last_login:
            instance.userloginhistory_set.create(login_time=instance.last_login)

models.signals.pre_save.connect(call_on_login, sender=User)