from django.apps import apps
from django.template import Library
import types
from data import APP_NAME
register = Library()


class ForeignKeyTypeDoesntExistError(Exception):
    message = "The model doesn't contain a ForeignKey of that type."


class MultipleForeignKeyError(Exception):
    message = "The model of one the search_fields contains ForeignKeys of the same type."


class ListRowData:
    id = None
    data = None


@register.inclusion_tag('rainbow/includes/model_table.html')
def show_model_table(table_data):
    return {"model_name": table_data.model_name,
            "columns": table_data.output_fields,
            "table_data": table_data.data}


@register.inclusion_tag('rainbow/includes/search_result_table.html')
def show_search_result_table(query_data):
    final_results = []
    count_total = 0
    main_model = query_data.model_type

    if query_data.query:
        raw_result = []
        for fk_field in query_data.search_fields:
            if "." in fk_field:
                query_model_name, field_name = fk_field.split(".")
                query_model = apps.get_model(APP_NAME, query_model_name)

                related_key_name = find_first_foreign_key_name(query_model, main_model)

                #TODO find way to allow this with multiple related_names
                fk_query_dict = {field_name + "__contains": query_data.query}
                count_for_field = query_model.objects.filter(**fk_query_dict).count()
                count_total += count_for_field

                last_set = 0
                last_row = 0

                if count_for_field < query_data.max_entries:
                    db_accesses_needed = 1
                else:
                    db_accesses_needed = count_for_field//query_data.max_entries

                for query_passes in range(db_accesses_needed):
                    start_row = query_data.max_entries*query_passes
                    end_row = query_data.max_entries*(query_passes+1)
                    for dupe_offset, row in enumerate(query_model.objects.filter(**fk_query_dict)[start_row: end_row]):
                        raw_result.append(getattr(row, related_key_name))
                        if len(raw_result) >= query_data.max_entries:
                            last_row = dupe_offset
                    raw_result = list(set(raw_result))
                    if len(raw_result) >= query_data.max_entries:
                            break
                    last_pass = start_row

                #Todo: Remove (or not) when multiple result pages need to be implemented
                next_index = last_set + last_row
                # raw_result = find_first_foreign_key_name(query_model, related_key_name, )
        for row in raw_result:
            final_row = []
            for fk_field in main_model.displayed_fields:
                data = getattr(row, fk_field)
                if isinstance(data, types.MethodType):
                    final_row.append(data())
                else:
                    final_row.append(data)
            final_data = ListRowData()
            final_data.id = row.id
            final_data.data = final_row
            final_results.append(final_data)

    return {
        "no_query": len(query_data.query) <= 0,
        "model_data": final_results,
        "columns": query_data.output_fields,
        "initial_msg": query_data.messages.initial,
        "error_msg": query_data.messages.no_results,
        "app_name": main_model._meta.app_label,
        "obj_type": main_model.__name__.lower(),
        "count_displayed": len(final_results),
        "count_total": count_total
    }


def find_first_foreign_key_name(model_type, target_fk_type):
    for fk_field in model_type._meta.get_fields():
        if fk_field.get_internal_type() == "ForeignKey" and isinstance(target_fk_type(), fk_field.rel.to):
            return fk_field.name
    raise ForeignKeyTypeDoesntExistError("ForeignKeyTypeDoesntExistError: Can't find the a foreign key "
                                         "with the type ({}) in the model ({}).".format(target_fk_type.__name__,
                                                                                        model_type.__name__))
