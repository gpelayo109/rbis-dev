from rainbow_db import version
from django.template import Library, Node


class VersionNode(Node):
    def render(self, context):
        context['version'] = version.get_version_code()
        return ''


register = Library()


@register.tag
def load_version_tags(parser, token):
    return VersionNode()
