"""rainbow_db URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^asview/(?P<model_name>[a-z0-9A-Z_]+)/(?P<pk>[0-9]+)', views.details, name='details'),
    url(r'media/', views.show_image, name='image'),
    url(r'^mrkttools/remarkstat/$', views.remark_stat, name='remarkstat'),
    url(r'^usertools/phonenumbersearch$', views.phone_number_search_view, name='phone_search'),
    url(r'^usertools/quickimport$', views.quick_import, name='quick_import'),
    url(r'^usertools/quickimport/preview$', views.quick_import_preview, name='quick_import_preview'),
    url(r'^usertools/quickimport/confirmed$', views.quick_import_confirmed, name='quick_import_confirmed')
]
