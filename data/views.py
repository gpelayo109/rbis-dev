from django.apps import apps
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.db import models
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from rainbow_db.settings import BASE_DIR
from data.models import Referral, DealerClientPair, Associate, PhoneNumber, Client, ServiceAppointment, ADMIN_BASE_EDIT
from data.data_builders import rmrkstat_builder, model_list_builder
from data.data_builders.qckimprt_builder import TextGodsownParser, DjangoORMLoader, QuickImportUtils
from data.data_builders.model_list_builder import ModelTableDataFactory
from rainbow_db import settings
import os

#Todo add user info breadcrumbs

skipped = ["Id"]
demoted = ["Date Created"]
COMMENT_APP = 'django_comments'
DATA_APP_NAME = 'data'

CAN_MODERATE = 'can_moderate'


#TODO:   if parameters (self, request) are needed
def show_details_view(self, request, queryset):
    model = queryset.model.__name__.title()
    model_id = str(queryset[0].id)
    return HttpResponseRedirect('/asview/{}/{}'.format(model, model_id))


def show_image(request):
    with open(BASE_DIR + request.path, 'rb') as image_file:
        return HttpResponse(image_file.read(), content_type="image/jpeg")


def remark_stat(request):
    data_args = rmrkstat_builder.get_data_args(request)
    template_loc = os.path.join(BASE_DIR, 'templates', 'rainbow', 'remark_stat.html')
    return render(request, template_loc, data_args)


def phone_number_search_view(request):
    search_parameters = ["PhoneNumber.number"]
    title = "Phone Number Search"
    initial_msg = "Kindly search for a phone number using the search box above."
    no_results_msg = "Sorry, can't find any phone numbers matching your query."
    data_args = model_list_builder.get_data_args(request, Client, search_parameters,
                                                 title=title,
                                                 initial_msg=initial_msg,
                                                 no_results_msg=no_results_msg)
    template_loc = os.path.join(BASE_DIR, 'templates', 'rainbow', 'phone_search.html')
    return render(request, template_loc, data_args)


def quick_import(request):
    last_excel_type = request.session.get("excel_type", None)
    no_excel_types_msg = "I'm sorry. We are unable to quick import any data. " \
                         "No Excel Type were set. Please Contact the Admin."

    #TODO: Get excel types by checking all files in /data/data_builders/settings/excel-types

    data_args = {"title": "Quick Import",
                 "excel_types": QuickImportUtils.get_excel_type_names(),
                 "no_excel_types_msg": no_excel_types_msg,
                 "selected_excel_type": last_excel_type}



    template_loc = os.path.join(BASE_DIR, 'templates', 'rainbow', 'quick_import.html')
    return render(request, template_loc, data_args)


#Todo Make model_data_list serielizable
def quick_import_preview(request):
    no_new_data_msg = "Sorry. The data you given either doesn't have any val" \
                      "id data or the data is already in the database. " \
                      "Press Back in your browser to try again"
    excel_type = request.POST.get('excel_type', None)
    data = request.POST.get('data', None)
    header_file = None
    #TODO make headers option later
    if excel_type == "Demo Schedule":
        header_file = "DEMOSCHED-2016"
    elif excel_type == "Leads":
        header_file = "LEADS-2016"

    header_data = QuickImportUtils.get_header_data(header_file)
    request.session['raw_data'] = data
    request.session['header_data'] = header_data
    request.session['excel_type'] = excel_type

    parser = TextGodsownParser(QuickImportUtils.get_xlsttg_filepath(excel_type), data, header_data,
                               origin="Quick Import")
    try:
        model_data_list = parser.create_data()
    except (ValueError, TypeError) as e:
        messages.add_message(request, messages.ERROR, "Excel Data doesn't match the excel type. Please check if "
                                                      "selected the correct excel type or the whole row was copied.")

        return redirect(quick_import)
    display_headers = dict()

    #TODO: move to excel_type ini under [Display Fields]
    #TODO: I soooo sorry
    if excel_type == "Leads":
        display_headers.setdefault("data.client", ["first_name", "last_name", "file_origin", "street", "city",
                                                   "best_time"])
        display_headers.setdefault("rainbow_remarks.remark", ["date_posted", "object_pk", "comment"])
        display_headers.setdefault("data.referral", ["date_created", "client", "relationship", "referred_by"])
        display_headers.setdefault("data.dealerclientpair", ["date_assigned", "dealer", "client"])
        display_headers.setdefault("data.phonenumber", ["client", "number"])
        display_headers.setdefault("data.associate", ["first_name"])
        display_headers.setdefault("data.city", ["name"])
    elif excel_type == "Demo Schedule":
        display_headers.setdefault("data.client", ["first_name", "last_name", "file_origin", "street", "city",
                                                   "best_time"])
        display_headers.setdefault("rainbow_remarks.remark", ["date_posted", "object_pk", "comment"])
        display_headers.setdefault("data.referral", ["date_created", "client", "relationship", "referred_by"])
        display_headers.setdefault("data.servicestatus", ["name"])
        display_headers.setdefault("data.serviceappointment", ["date_created", "client", "demo_date", "demo_time",
                                                               "created_by", "service_type", "call_in", "call_out",
                                                               "leads_given", "service_status",
                                                               "program_joined", "demo_done_by", "assistant"])
        display_headers.setdefault("data.servicetype", ["name"])
        display_headers.setdefault("data.associate", ["first_name"])
        display_headers.setdefault("data.city", ["name"])
        display_headers.setdefault("data.program", ["name"])

    mdl_factory = ModelTableDataFactory(model_data_list, display_headers)
    table_data_list = mdl_factory.create_set()
    if not table_data_list:
        if mdl_factory.skipped_row_count > 0:
            messages.add_message(request, messages.ERROR, "All data given is already in the database. "
                                                          "Please check again.")
        else:
            messages.add_message(request, messages.ERROR, "The data given was invalid, possibly from not copying the "
                                                          "whole row. Please check your data.")
        return redirect(quick_import)

    data_args = {"title": "Quick Import",
                 "no_new_data_msg": no_new_data_msg,
                 "excel_types": excel_type,
                 "table_data_list": table_data_list}
    template_loc = os.path.join(BASE_DIR, 'templates', 'rainbow', 'quick_import_preview.html')
    return render(request, template_loc, data_args)


def quick_import_confirmed(request):
    table_data = request.session.get("raw_data", None)
    header_data = request.session.get("header_data", None)
    excel_type = request.session.get("excel_type", None)
    parser = TextGodsownParser(QuickImportUtils.get_xlsttg_filepath(excel_type), table_data, header_data)
    model_data_list = parser.create_data()

    for mdl_data in model_data_list:
        loader = DjangoORMLoader()
        loader.save(mdl_data)

    return HttpResponseRedirect(reverse("quick_import"))

SERVICE_APPOINTMENT_PATH = ServiceAppointment.__name__.lower()
CLIENT_PATH = Client.__name__.lower()


class TemplateModelData:
    name = ""
    value = ""


def details(request, model_name, pk):
    def format_referral_data(referral_data):
        referral_list = []
        for referral in referral_data:
            dc_pairs = DealerClientPair.objects.filter(client=referral.referred_by)
            if dc_pairs:
                dealer_list = []
                for pair in dc_pairs:
                    dealer_list.append(Associate.objects.get(pk=pair.dealer_id))
                referral.dealers = ", ".join([str(dealer_row) for dealer_row in dealer_list])
            referral_list.append(referral)
        return referral_list

    model = apps.get_model(app_label='data', model_name=model_name)
    queryset = model.objects.filter(pk=pk)
    model = queryset.model
    obj = queryset[0]
    if hasattr(obj, 'field_order'):
        data_list = ['@blank@']*len(obj.field_order)
    else:
        data_list = []

    if CLIENT_PATH == model_name:
        client = obj
    else:
        client = None

    field_list = model._meta.fields + model._meta.many_to_many

    for field in field_list:
        field_data = TemplateModelData()
        field_data.name = field.verbose_name.title()
        field_data.value = getattr(queryset[0], field.name)
        if isinstance(field_data.value, bool):
            field_data.value = "Yes" if field_data.value else "No"
        if isinstance(field, models.ManyToManyField):
            field_data.value = ', '.join(map(str, getattr(queryset[0], field.name).all()))
        if field_data.value is None:
            field_data.value = ''
        if field_data.name not in skipped:
            if hasattr(obj, 'field_order') and field.name in obj.field_order:
                index = obj.field_order.index(field.name)
                data_list[index] = field_data
            else:
                data_list.append(field_data)

        #Client Hack
        if 'client' == field.name:
            client = queryset[0].client
        #TODO: make modulaer method for replacing values
        elif 'marital_status' == field.name:
            if field_data.value == 'm':
                field_data.value = 'Married'
            elif field_data.value == 's':
                field_data.value = 'Single'
            else:
                field_data.value = 'Unknown'

    #Moves demoted fields to the end of the list
    if not hasattr(obj, 'field_order'):
        demote_data = [data_list.pop(i) for i in range(len(data_list)-1) if data_list[i].name in demoted]
        data_list += demote_data
    data_str = str(queryset[0])
    has_moderation = request.user.has_perm(COMMENT_APP + "." + CAN_MODERATE)
    dealer_data = []
    client_pk = None

    if model_name == CLIENT_PATH:
        client_pk = pk
    elif model_name == SERVICE_APPOINTMENT_PATH:
        client_pk = obj.client.pk

    raw_refer_from_data = Referral.objects.filter(client=client_pk).order_by("-date_created")
    raw_refer_to_data = Referral.objects.filter(referred_by=client_pk).order_by("-date_created")
    refer_from_data = format_referral_data(raw_refer_from_data)
    refer_to_data = format_referral_data(raw_refer_to_data)
    phone_number_data = [phone_number.number for phone_number in PhoneNumber.objects.filter(client=client_pk)]
    dealerclientpairs = DealerClientPair.objects.filter(client=client_pk).order_by("-date_assigned")
    for pair in dealerclientpairs:
        dealer = Associate.objects.get(pk=pair.dealer_id)
        dealer_data.append({'name': str(dealer), 'pk': pair.dealer_id, 'date_assigned': pair.date_assigned})

    data_args = {
        'title': "Now Viewing {} {}".format(model_name, data_str),
        'opts': model._meta,
        'data_list': data_list,
        'obj': obj,
        'client': client,
        'obj_str': str(queryset[0]),
        'refer_from_data': refer_from_data,
        'refer_to_data': refer_to_data,
        'dealer_data': dealer_data,
        'can_moderate': has_moderation,
        'has_permission': request.user.is_authenticated(),
        'can_edit': request.user.has_perm(DATA_APP_NAME + "." + ADMIN_BASE_EDIT),
        'username': str(request.user),
        'phone_numbers': phone_number_data
    }

    template_loc = os.path.join(BASE_DIR, 'templates', 'rainbow', 'details.html')
    return render(request, template_loc, data_args)