import os
import configparser

SITE_ID = 1
# Sets (in minutes) when the user will be logged out after the user is idle
# If AUTO_LOGOUT_IDLE is 5 then the user will logout after 5 minutes.
# If AUTO_LOGOUT_IDLE is -1 then there is no auto logout after idle
AUTO_LOGOUT_IDLE = -1

# Sets time after the user will be logged out after the user is idle
# Time format is 24-hour and HHMM
# If SCHEDULED_LOG0UT_TIME is 14 then the user will be logout on 2:00pm.
# If SCHEDULED_LOG0UT_TIME is -1 then there is no scheduled auto logout
SCHEDULED_LOG0UT_TIME = 0

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

config = configparser.RawConfigParser()

try:
    config.read_file(open(os.path.join(BASE_DIR, 'rainbow_db', 'servers.ini')))
except FileNotFoundError:
    print("\nStopping Server...\n Could not find  a valid server file named "
          "servers.ini in the same folder of rainbows settings.py")
    quit()

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config.get("Security", "key")
# SECURITY WARNING: don't run with debug turned on in production!

debug_opt_str = config.get("Dev", "debug").lower()

if "false" == debug_opt_str:
    DEBUG = False
elif "true" == debug_opt_str:
    DEBUG = True
else:
    DEBUG = None

ALLOWED_HOSTS = config.get("Security", "allowed_hosts").split(",")

FIXTURE_DIRS = ['base_data/fixtures']

# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'data',
    'daterange_filter',
    'django_comments',
    'adminactions',
    'rainbow_remarks',
    'autocomplete_light',
)

COMMENTS_APP = 'rainbow_remarks'
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'data.middleware.ScheduleLogout',
    'data.middleware.Stopwatch'
)

ROOT_URLCONF = 'rainbow_db.urls'

CACHES = {
    'default': {
        'BACKEND': "django.core.cache.backends.memcached.MemcachedCache",
        'LOCATION': '127.0.0.1:11211',
    }
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'),
                 os.path.join(BASE_DIR, 'static')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'rainbow_db.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config.get('Database', 'name'),
        'USER': config.get('Database', 'user'),
        'PASSWORD': config.get('Database', 'password'),
        'HOST': config.get('Database', 'host'),
        'PORT': config.get('Database', 'port'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Manila'

USE_I18N = True

USE_L10N = True

USE_TZ = True

DATE_FORMAT = "mdyHM"

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static")
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

MEDIA_URL = '/media/'