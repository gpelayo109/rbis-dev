DATABASE = "4"
SYSTEM = "9"
BUGFIXES_OR_MINORFEAT = "2"
INTERNAL = "0"

IS_NIGHTLY = False
NIGHTLY_CODE = 0


def get_version_code():
    version_code = "D{}S{}-{}-{}".format(DATABASE, SYSTEM, BUGFIXES_OR_MINORFEAT, INTERNAL)

    if IS_NIGHTLY:
        version_code = "{} - N{}".format(version_code, NIGHTLY_CODE)

    return version_code

