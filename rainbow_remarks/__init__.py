from rainbow_remarks.models import Remark
from rainbow_remarks.form import RemarksForm


def get_model():
    return Remark


def get_form():
    return RemarksForm