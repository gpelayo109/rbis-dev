from django import forms
from django_comments.forms import CommentForm
from django_comments import signals
from rainbow_remarks.models import Remark


class RemarksForm(CommentForm):
    date_posted = forms.HiddenInput()

    def __init__(self, *args, **kwargs):
        super(RemarksForm, self).__init__(*args, **kwargs)
        self.fields.pop('name')
        self.fields.pop('email')
        self.fields.pop('url')
        self.fields.pop('honeypot')

    def get_comment_model(self):
        return Remark

    def clean(self):
        self.cleaned_data['email'] = ''
        self.cleaned_data['url'] = ''
        self.cleaned_data['name'] = ''
        return self.cleaned_data

    def get_comment_create_data(self):
        data = super(RemarksForm, self).get_comment_create_data()
        return data