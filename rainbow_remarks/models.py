from django.db import models
from django.contrib.auth.models import ContentType
from django_comments.models import Comment
from django.utils import timezone


class RemarkType(models.Model):
    name = models.CharField(max_length=35)
    abbreviation = models.CharField(max_length=10)

    def __str__(self):
        return "[{}] {}".format(self.abbreviation, self.name)

    class Meta:
        app_label = 'rainbow_remarks'


class Remark(Comment):
    date_posted = models.DateTimeField(default=timezone.now)
    type = models.ForeignKey(RemarkType, null=True, blank=True)
    data_origin = models.CharField(max_length=35, default="local")
    _is_empty = False

    @staticmethod
    def get_latest_remark(model):
        content_type = ContentType.objects.get_for_model(model)
        remark = Remark.objects.filter(content_type=content_type, object_pk=model.pk, is_removed=False)\
            .order_by('-date_posted').first()
        if not remark:
            remark = Remark()
            remark._is_empty = True
        return remark

    def is_empty(self):
        return self._is_empty

    class Meta:
        app_label = 'rainbow_remarks'
