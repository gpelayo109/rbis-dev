from rainbow_remarks.models import Remark, RemarkType
from data.models import Client
from django.contrib.auth.models import ContentType
from django.template import Library, Node, Variable


class RemarkNode(Node):
    client_id_var = None

    def __init__(self, client_id_var):
        self.client_id_var = Variable(client_id_var)

    def render(self, context):
        client_ctype = ContentType.objects.get_for_model(Client)
        client_id = self.client_id_var.resolve(context)
        remarks = Remark.objects.filter(content_type=client_ctype, object_pk=client_id, is_removed=False)
        context['has_remarks'] = len(remarks) > 0
        context['get_remarks'] = remarks
        return ''


class RemarkTypeNode(Node):
    def render(self, context):
        remarks = RemarkType.objects.all()
        context['get_remark_types'] = remarks
        return ''


register = Library()
node_factory = None


@register.tag
def load_remarks(parser, token):
    client_id_var = token.split_contents()[1]
    return RemarkNode(client_id_var)


@register.tag
def load_remark_types(parser, token):
    return RemarkTypeNode()
