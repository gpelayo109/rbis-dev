from django.conf.urls import url
from rainbow_remarks import views

app_name = 'rainbow_remarks'

urlpatterns = [
    url(r'add/(?P<client_id>[0-9]+)', views.post_client_remark, name='add_remark'),
    url(r'posted/', views.redirect_to_referrer, name='comment_posted'),
    url(r'delete/(?P<remark_id>[0-9]+)', views.delete_remark, name='delete_remark'),
]