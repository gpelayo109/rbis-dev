from rainbow_remarks.models import Remark, RemarkType
from data.models import Client
from django.contrib.auth.models import ContentType
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404
from django_comments.models import Site
from django.utils import timezone

client_ctype = ContentType.objects.get_for_model(Client)


def post_client_remark(request, client_id):
    refer_url = request.META.get('HTTP_REFERER', None)
    new_remark = Remark()
    new_remark.user = request.user
    new_remark.content_type = client_ctype
    new_remark.site = Site.objects.get_current(request)
    new_remark.submit_date = timezone.now()
    new_remark.comment = request.POST['text']
    new_remark.is_public = True
    new_remark.object_pk = client_id
    remark_type_id = int(request.POST['type_id'])
    print("$"*12 + str(RemarkType.objects.get(id=remark_type_id)))
    new_remark.type = RemarkType.objects.get(id=remark_type_id)
    new_remark.clean()
    new_remark.save()
    return HttpResponseRedirect(refer_url)


def delete_remark(request, remark_id):
    remark = get_object_or_404(Remark, id=remark_id)
    if remark.user.id != request.user.id and not request.user.is_superuser:
        print("[DeleteComment] Unauthorized Delete Attempt By %s" % request.user)
        raise Http404
    else:
        remark.is_removed = True
        remark.save()
        return redirect_to_referrer(request)


def redirect_to_referrer(request):
    refer_object = request.META.get('HTTP_REFERER', None)
    return HttpResponseRedirect(refer_object)