from rainbow_db import settings
import os
import codecs

__author__ = 'Geryl'

should_write = True


class Stopper:
    SKIP_TEXT = 's'

    def __init__(self, default_state):
        self.can_pause = default_state

    def attempt_stop(self):
        if self.can_pause:
            input_data = input("PAUSED (Type {} and press ENTER to disable pausing)\n".format(self.SKIP_TEXT))
            if input_data == self.SKIP_TEXT:
                self.can_pause = False
                print("Now Skipping All Pauses.")


class LuzLogger:
    DEBUG_DATA_FOLDER = os.path.join(settings.BASE_DIR, "debug-data")

    file = None
    file_path = None
    type_file_name_dict = None
    default_file_name = ""
    last_content = None
    repeat_count = 0
    @staticmethod
    def quickly_write_line(default_file_name, content):
        quick_file = LuzLogger(default_file_name)
        quick_file.write_line(content)
        quick_file.close()

    def __init__(self, file_name):
        if not os.path.isdir(self.DEBUG_DATA_FOLDER):
            os.mkdir(self.DEBUG_DATA_FOLDER)
        self.file_path = os.path.join(self.DEBUG_DATA_FOLDER, file_name)
        self.last_type_opened = None
        self.type_file_name_dict = {}
        self.default_file_name = file_name
        self.file = codecs.open(self.file_path, "a+", 'utf-8')

    def add_type(self, log_type, file_name):
        self.type_file_name_dict[log_type] = file_name

    def safely_open(self, log_type):
        if self.last_type_opened == log_type:
            if self.file.closed:
                self.file = open(self.file_path, "a+")
        else:
            if log_type:
                self.last_type_opened = log_type
                self.file_path = os.path.join(self.DEBUG_DATA_FOLDER, self.type_file_name_dict[log_type])
            else:
                self.last_type_opened = None
                self.file_path = os.path.join(self.DEBUG_DATA_FOLDER, self.default_file_name)
            self.file = open(self.file_path, "a+")

    def close(self):
        if self.file is not None:
            self.file.close()

    def _safely_write(self, content):
        if content == self.last_content:
            self.repeat_count += 1
            self.file.write("Repeated Last Message {} more times.".format(self.repeat_count))
        try:
            self.file.write(content)
        except Exception as e:
            self.file.write("Unable to Write Data Due To Exception: {}".format(str(e)))

    def write_line_to_all(self, content):
        type_filenames = self.type_file_name_dict.values()
        default_file_path = os.path.join(self.DEBUG_DATA_FOLDER, self.default_file_name)
        file_paths = [os.path.join(self.DEBUG_DATA_FOLDER, file_path) for file_path in type_filenames] + \
                     [default_file_path]
        for all_file_name in file_paths:
            self.file = codecs.open(all_file_name, "a+", "utf-8")
            self._safely_write(content)
            self._safely_write("\n")
            self.file.close()

    def write_line(self, content, type=None):
        self.safely_open(type)
        if self.file is not None and should_write:
            self._safely_write(content)
            self._safely_write("\n")
            self.file.close()
