import xlwt
from django.contrib.auth.models import *
from django.http import HttpResponse


class ExcelBuilder:
    def create_response(self, queryset, response):
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet("Service Appointments")

        row_num = 0

        #Warning only gets fields excel-types of fields
        # 7/11/2016 I forgot what this means
        fields = queryset[0]._meta.fields + queryset[0]._meta.many_to_many

        header_font = xlwt.XFStyle()
        header_font.font.bold = True

        for col_num in range(len(fields)):
            title = fields[col_num].verbose_name.title()
            ws.write(row_num, col_num, title, header_font)

            char_width = 300

            ws.col(col_num).width = self.get_width(fields[col_num])

            field_name_length = len(title) * char_width
            if ws.col(col_num).width < field_name_length:
                ws.col(col_num).width = field_name_length

        font_style = xlwt.XFStyle()
        font_style.alignment.wrap = 1
        for obj in queryset:
            row_num += 1

            for col_num in range(len(fields)):
                raw_field_data = getattr(obj, fields[col_num].name)
                if raw_field_data is None:
                    field_data = ""
                elif isinstance(fields[col_num], models.DateTimeField):
                    field_data = raw_field_data.strftime('%Y-%m-%d %H:%M')
                elif isinstance(fields[col_num], models.BooleanField):
                    if raw_field_data:
                        field_data = 'Yes'
                    else:
                        field_data = 'No'
                elif isinstance(fields[col_num], models.ManyToManyField):
                    if raw_field_data.count() > 0:
                        field_data = str("; ".join(map(str, raw_field_data.all())))
                    else:
                        field_data = ""
                else:
                    field_data = str(raw_field_data)

                ws.write(row_num, col_num, field_data, font_style)

        wb.save(response)
        return response

    #Gets Recommended Width Base on Model Type
    def get_width(self, field):
        CHAR_WIDTH = 300

        if isinstance(field, (models.AutoField, models.IntegerField)):
            return 10 * CHAR_WIDTH
        elif isinstance(field, models.DateTimeField):
            return 16 * CHAR_WIDTH
        elif isinstance(field, models.ForeignKey):
            return 15 * CHAR_WIDTH
        elif isinstance(field, models.BooleanField):
            return 3 * CHAR_WIDTH
        elif isinstance(field, models.ManyToManyField):
            return 17 * CHAR_WIDTH
        elif isinstance(field, models.IntegerField):
            return 10 * CHAR_WIDTH
        else:
            return 17 * CHAR_WIDTH


class ExcelCurryCook:
    filename = "excel.xls"

    def __init__(self, filename="excel.xls", *args, **kwargs):
        self.filename = filename

    def curry_export_action(self):
        def export_excel(model_admin, request, queryset):
            response = HttpResponse(content_type='application/ms-excel')
            response['Content-Disposition'] = 'attachment; filename={}'.format(self.filename)
            excel_builder = ExcelBuilder()
            return excel_builder.create_response(queryset, response)
        export_excel.short_description = "Export Data to Excel File"
        return export_excel
