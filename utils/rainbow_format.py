import re
from enum import Enum
import xlrd
from xlrd import xldate

from django.utils.timezone import datetime
from datetime import date
import pytz

DEFAULT_CENTURY = "20"


class InvalidDateFormat(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class PhoneNumberPack:
    contact_number = None
    alt_mobile_number = None
    alt_landline = None


def create_tz_date(month, day, year, timezone):
    date = datetime(year, month, day)
    return pytz.timezone(timezone).localize(date)


class DateOrder(Enum):
    US = 1
    Intl = 2
    ISO = 3


def convert_to_tz_date(date, timezone, order=DateOrder.Intl, default_year=None, format=None):
    valid_regex = "[0-9/]+"
    if order == DateOrder.Intl:
        day_index = 0
        month_index = 1
        year_index = 2
    else:
        raise InvalidDateFormat("Invalid Order")
    if format:
        return datetime.strptime(date, format).date()
    elif isinstance(date, str):
        if not date:
            raise InvalidDateFormat("Date is an empty str")

        fixed_date_str = date
        invalid_chars = re.sub(valid_regex, "", date)
        date_array = fixed_date_str.replace("-", "/").split("/")
        if date_array[month_index].isalpha():
            date_array[month_index] = str(datetime.strptime(date_array[month_index], "%b").month)
        else:
            date_array[month_index] = date_array[month_index]

        try:
            if len(date_array[year_index]) == 2:
                date_array[year_index] = "{}{}".format(DEFAULT_CENTURY, date_array[year_index])
        except IndexError:
            date_array.append(default_year)

        invalid_char_str = "".join(set(invalid_chars)).replace("\\", "\\\\").replace(".", "\.").replace("-", "\-")

        if invalid_char_str:
            for date_sect in date_array:
                re.sub("[%s]+" % invalid_char_str, "", date_sect)

            date_array = [int(re.sub("[%s]+" % invalid_char_str, "", date_sect)) for date_sect in date_array]

        if len(date_array) == 3:
            try:
                fixed_date_str = "{:04d}{:02d}{:02d}".format(date_array[year_index], date_array[month_index], date_array[day_index])
            except ValueError as e:
                raise InvalidDateFormat(str(e))
        elif len(date_array) < 3:
            raise InvalidDateFormat("Date ({}) is not complete. Date Array: {}".format(date, date_array))
        else:
            raise InvalidDateFormat("Date ({}) contains too much data. Formatted Date Array: {}".format(date, date_array))

    else:
        try:
            date_tuple = xlrd.xldate_as_tuple(date, 0)
            fixed_date_str = "{:04d}{:02d}{:02d}".format(date_tuple[0], date_tuple[1], date_tuple[2])
        except xlrd.xldate.XLDateAmbiguous:
            raise InvalidDateFormat("Date ({}) tuple causes Leap 1900 Year Problem.".format(date))

    if not fixed_date_str:
        raise InvalidDateFormat("Date Doesn't Contain Any Numbers or Slashes")
    try:
        final_datetime = datetime.strptime(fixed_date_str, "%Y%m%d")
        final_datetime = pytz.timezone(timezone).localize(final_datetime)
    except ValueError as e:
        raise InvalidDateFormat(str(e))
    return final_datetime.date()


def format_names(dealer_name):
    dealer_name_list = dealer_name.strip().split(" ")
    first_name = " ".join(dealer_name_list[:len(dealer_name_list)-1]).lower().strip().title()
    last_name = dealer_name_list[len(dealer_name_list)-1].lower().strip().title()

    if last_name and not first_name:
        first_name = last_name
        last_name = ""
    elif not last_name:
        last_name = ""

        if not first_name:
            first_name = ""

    return first_name, last_name


def extract_parentheses(value_with_para, remarks_builder=None, client_id=-1):
    para_name_list = value_with_para.split('(')
    fixed_value = para_name_list[0].strip()
    for para_name in para_name_list[1:len(para_name_list)]:
        if remarks_builder and client_id != -1:
            remarks_builder.update(client_id, para_name, msg_prefix="Notes about Name: ")
        pass
    return fixed_value


def process_phone_numbers(contact_numbers, remarks_builder=None, client_id=-1):
    """
    Converts an list of strings or numbers to valid phone numbers.
    In the first loop, if the element contains a phone_number_delimiter, it seperate the numbers
    into multiple strings.
    Ex:
        Where delimiter = '/'
        [4151234565/5105436543] ->  [4151234565, 5105436543]

    Then, it converts each element to to a string data type (if the elements are floats or integers)
    and appends it to the split_numbers list

    In the second loop, it enumerates the split_numbers list to check any remarks are in the phone number strings.
    The string is split into multiple sections where the dividing index is at the remarks_delimiter.
    The section without any numbers is will be the data used to create a remark if the builder and client id was given.
    The any section containing numbers will be formatted, remerged, and added to the formatted numbers list to be returned.

    Currently in the final formatting...
        removing any parentheses, dashes and spaces
        the decimals are removed
        a zero is appended if the number starts with a 9 (The first digit to all Filipino cellphone numbers)

    :param contact_numbers:
    A list of strings, integers, etc. containing possible phone numbers
    :param remarks_builder
    The builder used to create the remarks
    :param client_id:
    An optional param needed for creating remarks for a specific client (id) about its invalid phone numbers
    :return:
    A list of strings containing all valid phone numbers found in the contact_numbers list
    """

    phone_number_delimiter = '/'
    remarks_delimiter = '('
    remark_prefix = "Phone Number Remark: "
    split_numbers = []
    formatted_numbers = []

    if isinstance(contact_numbers, str):
        contact_numbers = [contact_numbers]

    for phn_num in contact_numbers:
        if phn_num:
            if isinstance(phn_num, str):
                split_list = phn_num.split(phone_number_delimiter)

                if len(split_list) > 1:
                    split_numbers += [split_num.strip() for split_num in split_list if split_num]
                else:
                    split_numbers.append(phn_num.strip())
            else:
                split_numbers.append(str(phn_num))

    for raw_num in split_numbers:
        try:
            str_num = ""
            para_num_list = raw_num.split(remarks_delimiter)
            for para_num in para_num_list:
                formatted_para_num = re.sub("[\(\)-]", '', para_num)
                if re.search("[0-9]", formatted_para_num):
                    str_num += formatted_para_num.replace(" ", "")
                else:
                    if client_id != -1 and remarks_builder:
                        remarks_builder.update(client_id, formatted_para_num, msg_prefix=remark_prefix)
                    else:
                        print("Warning: Need Client Id to create Remark: %s" % formatted_para_num)

            final_num = str(int(float(str_num)))
        except ValueError:
            final_num = raw_num.strip()

        if final_num[0] == '9' and len(final_num) == 10:
            final_num = "0" + final_num
        formatted_numbers.append(final_num)
    return formatted_numbers


def organize_client_numbers(contact_number_list, remarks_builder=None, client_id=-1):
    phone_numbers = PhoneNumberPack()

    if len(contact_number_list) == 1:
        phone_numbers.contact_number = contact_number_list[0]
    else:
        for phn_num in contact_number_list:
            if not phone_numbers.contact_number:
                phone_numbers.contact_number = phn_num
            elif len(phn_num) > 1 and phn_num[1] == '9' and not phone_numbers.alt_mobile_number:
                phone_numbers.alt_mobile_number = phn_num
            elif not phone_numbers.alt_landline:
                phone_numbers.alt_landline = phn_num
            else:
                if remarks_builder and client_id != -1:
                    remarks_builder.update(client_id, phn_num, msg_prefix="Extra Numbers: ")
    return phone_numbers


def linux_date_to_django_date(date):
    try:
        date_tuple = xlrd.xldate_as_tuple(date, 0)
        return "{:04d}-{:02d}-{:02d}".format(date_tuple[0], date_tuple[1], date_tuple[2])
    except xldate.XLDateAmbiguous:
        print("Date Too Ambiguious: %s" % date)


def linux_time_to_django_time(time):
    try:
        time_tuple = xlrd.xldate_as_tuple(time, 0)
        return "{:02d}:{:02d}".format(time_tuple[3], time_tuple[4])
    except xldate.XLDateAmbiguous:
        print("Time Too Ambiguious: %s" % time)


def linux_time_to_django_datetime(time):
    try:
        datetime_tuple = xlrd.xldate_as_tuple(time, 0)
        return "{:04d}-{:02d}-{:02d} {:02d}:{:02d}".format(datetime_tuple[0], datetime_tuple[1], datetime_tuple[2],
                                                           datetime_tuple[3], datetime_tuple[4])
    except xldate.XLDateAmbiguous:
        print("Time Too Ambiguious: %s" % time)


def linux_time_to_str(time):
    try:
        time_tuple = xlrd.xldate_as_tuple(time, 0)
        new_tuple = (1900, 1, 1, time_tuple[3], time_tuple[4], 0)
        #TODO Fix unreferenced datetime
        return datetime.datetime(*new_tuple).strftime("%I:%M %p")
    except xlrd.xldate.XLDateAmbiguous:
        print("Time Too Ambiguious: %s" % time)
